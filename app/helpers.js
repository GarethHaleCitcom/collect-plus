
export const app = (key, defaultValue = null) => {
    if(!isDOMAvailable()) {return []};
    const keys = key.split('.');
    let value = Object.assign({}, window.APP);

    for (let i = 0; i < keys.length; i++) {
        if (!value.hasOwnProperty(keys[i])) {
            return defaultValue;
        }
        value = value[keys[i]];
    }

    return value;
};

export const isDOMAvailable = () => {
  return process.browser;
};