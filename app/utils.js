import api from "./api";
import partners from "../config/partners";

export const fetchStoresFromBounds = async (bounds) => {
  return api(`${process.env.NEXT_PUBLIC_API_URL}location`,{bounds});
};

export const fetchPostcodePosition = async (postcode) => {
  return api(`${process.env.NEXT_PUBLIC_API_URL}postcode`,{postcode});
};

export const radians = number => (Math.PI / 180) * number;

export const distance = ({lat: lat1, lng: lon1}, {lat: lat2, lng: lon2}) => {
    if ((lat1 === lat2) && (lon1 === lon2)) {
        return 0;
    }
    const radLat1 = Math.PI * lat1 / 180;``
    const radLat2 = Math.PI * lat2 / 180;
    const theta = lon1 - lon2;
    const radTheta = Math.PI * theta / 180;
    let dist = Math.sin(radLat1) * Math.sin(radLat2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.cos(radTheta);
    if (dist > 1) {
        dist = 1;
    }
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    dist = dist * 0.8684;
    return parseFloat(dist.toFixed(2));
};

export const withinDistance = (store, location, d = 2.5) => {
    const storeDistance = distance({lat: store.latitude ? store.latitude : store.lat, lng: store.longitude ? store.longitude : store.lng}, location);
    return storeDistance <= d;
};

export const hasServices = (store, selectedServices) => {
    if (selectedServices.length === 0) {
        return true;
    }
    const storeServices = store.services.split(",");
    return storeServices.filter(service => selectedServices.indexOf(service) > -1).length === selectedServices.length;
};

export const hasPartners = (store, selectedPartners) => {
  if(selectedPartners.length === 0) {
    return true;
  }
  const storePartners = store.carriers;
  return storePartners.filter((partner) => selectedPartners.includes(partner)).length > 0;
};

export const isOpen = (store, opening) => {
    if (!opening) {
        return true;
    }
    const now = new Date();
    const time = parseInt(`${now.getHours()}${now.getMinutes()}`);
    const open = parseInt(store[`${store.today}_open`]);
    const close = parseInt(store[`${store.today}_close`]);
    if (opening === "now") {
        return (time >= open && time <= close);
    }
    if (opening === "allDay") {
        return (open === 1 && close === 2359);
    }
    return true;
};

export const filterStores = (stores, location, filters) => {
    const selectedServices = [];
    for (const service in filters.services) {
        if (!filters.services.hasOwnProperty(service)) {
            continue;
        }
        if (filters.services[service].selected) {
            selectedServices.push(service);
        }
    }

    return stores.filter(store => (
        withinDistance(store, location, filters.distance) &&
        hasServices(store, selectedServices) &&
        hasPartners(store, filters.carriers) &&
        isOpen(store, filters.opening)
    ));
};
