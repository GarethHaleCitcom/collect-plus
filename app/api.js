import requestApi from "superagent";
import get from "lodash/get";

export default (url, reqData = {}) => {
  return new Promise((resolve) => {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Origin: window.location.origin,
    };

    const req = requestApi.post(url).send(reqData);

    for (const header in headers) {
      if (!headers.hasOwnProperty(header)) {
        continue;
      }

      req.set(header, headers[header]);
    }

    let status = 520;
    let errors = [];
    let data = null;
    let ok = false;

    req.end((err, res) => {
      if (err) {
        console.log(err);
        errors = ['Server error'];
      }

      status = get(res, 'status', 520);
      data = get(res, 'body', {});
      ok = get(res, 'ok', false);

      if (!ok) {
        errors = ['Server error'];
      }

      resolve({status, data, errors, ok});
    });

    return {status, data, errors, ok};
  });
}
