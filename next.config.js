module.exports = {
  env: {
    NEXT_PUBLIC_GA_PA_KEY: process.env.GA_PA_KEY,
    PARCEL_ENDPOINT: process.env.PARCEL_ENDPOINT,
    GET_TOKEN_PATH: process.env.GET_TOKEN_PATH,
    GET_PARCEL_TRACKING: process.env.GET_PARCEL_TRACKING,
    NEXT_PUBLIC_GA_ID: process.env.GA_ID,
    NEXT_PUBLIC_API_URL: process.env.NEXT_PUBLIC_API_URL,
    GOOGLE_API_KEY: process.env.GOOGLE_API_KEY,
  },
};
