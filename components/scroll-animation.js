import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import clone from 'lodash';

export default function ScrollAnimation({children, className,delay, animClassName, viewPort, idSelector}) {
  let windowOffsetBottom = 0;
  let collections = [];
  let timer = null;


  const getOffsetTop = (element) => {
    let offsetTop = 0;
    while (element) {
      offsetTop += element.offsetTop;
      element = element.offsetParent;
    }
    return offsetTop;
  };

  const animate = () => {
    windowOffsetBottom = window.innerHeight + window.pageYOffset;
    collections.forEach((elem, index) => {
      let elemOffsetBottom = viewPort + getOffsetTop(elem);
      if (elem.classList.contains('Animation--Done') || elem.classList.contains(animClassName)) {
        return;
      }
      if (getOffsetTop(elem) < windowOffsetBottom) {
        if ((elemOffsetBottom - window.scrollY > 0) || (windowOffsetBottom > elemOffsetBottom)) {
          if (elem.classList.contains(animClassName)) {
            return
          }
          timer = setTimeout(() => {
            elem.classList.add(animClassName);
          }, index === 0 ? 0 : delay * index);
          elem.addEventListener('animationend', (e) => {
            // collections.splice(0, index);
            e.currentTarget.removeAttribute('id');
            // e.currentTarget.classList.remove(animClassName);
            elem.classList.add('Animation--Done');
            clearTimeout(timer);
          })
        }
      }
    });
  };

  const getElements = () => {
    collections = clone(document.querySelectorAll(idSelector));
    animate();
  };

  useEffect(() => {
    getElements();
    window.addEventListener('scroll', animate.bind(this), true);
  }, []);

  return (
    <div className={className}>
      {children}
    </div>
  );
}

ScrollAnimation.propTypes = {
  delay: PropTypes.number,
  animClassName: PropTypes.string,
  viewPort: PropTypes.number,
  idSelector: PropTypes.string,
};

ScrollAnimation.defaultProps = {
  delay: 100,
  viewPort: 80,
  animClassName: 'fade-in-on-scroll',
  idSelector: '#fade-in-on-scroll'
};
