import React from 'react';
import PropTypes from 'prop-types';
import ScrollAnimation from "./scroll-animation";

export default function StyledContent({title, description, subtitle, descriptionAsHtml = false}) {
  return (
    <div className="styled-content">
      {title && <h1 id="fade-in-on-scroll" className="styled-content-title">{title}</h1>}
      {subtitle && <h2 className="styled-content-subtitle">{subtitle}</h2>}
      {descriptionAsHtml && description && <div id="fade-in-on-scroll" className="styled-content-description" dangerouslySetInnerHTML={{__html: descriptionAsHtml && description}}/>}
      {!descriptionAsHtml && description && <p id="fade-in-on-scroll" className="styled-content-description">{description}</p>}
    </div>
  )
}

StyledContent.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  descriptionAsHtml: PropTypes.bool,
  subtitle: PropTypes.string,
};

StyledContent.defaultProps = {
  descriptionAsHtml: false,
  title: null,
  subtitle: null,
  description: null,
};
