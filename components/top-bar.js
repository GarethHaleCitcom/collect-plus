import React from 'react';
import MobileMenu from "./mobile-menu";
import Link from "next/link";

export default function TopBar() {
    return (
        <>
            <div className="top-bar">
                <div className="container-large">
                   <Link href="/">
                     <img className="top-bar-logo" src="/logo.svg"/>
                   </Link>
                </div>
            </div>
            <MobileMenu/>
        </>
    )
}
