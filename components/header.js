import React from 'react';
import PropTypes from 'prop-types';
import ServiceMessage from "./service-message";
import { Parallax, Background } from 'react-parallax';

export default function Header({children, backgroundImage, title}) {
    return (
      <Parallax
        blur={0}
        bgImage={backgroundImage}
        bgImageAlt=""
        strength={50}
      >
        <header className="header">
            <img className="header-graphic graphic--left" src="/graphics/header-gray-graphic.svg" alt="graphic"/>
            <img className="header-graphic graphic--right" src="/graphics/header-orange-graphic.svg" alt="graphic"/>
            <ServiceMessage message="As of the 1 October 2020 you may notice some small but significant changes with Collect+, but don’t worry you will still be able to use the Collect+ service that you know and love. PayPoint have taken full ownership of the Collect+ brand and will continue to service returns and click & collect across multiple carriers including Yodel, DHL, FedEx and also retail giants such as Amazon and eBay."/>
            <div className="container-large">
                <h1 className="header-title" dangerouslySetInnerHTML={{__html: title}}/>
            </div>
        </header>
      </Parallax>
    )
}

Header.defaultProps = {
  backgroundImage: PropTypes.string,
};
