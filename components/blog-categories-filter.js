import React, {useContext} from 'react';
import {AppContext} from "../config/context";

export default function BlogCategoriesFilter() {
  const {blogCategories} = useContext(AppContext);

  return (
    <div className="blog-categories-filter">
      <ul className="blog-categories-filter__list">
        {
          blogCategories.map(({name, id}, key) => {
            return (
              <li key={key} className={`blog-categories-filter__list-item ${id === 'all-posts' ? 'item--active' : ''}`}>{name}</li>
            )
          })
        }
      </ul>
    </div>
  )
}
