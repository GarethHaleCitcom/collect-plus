import React, {useContext, useRef, useEffect} from 'react';
import ScrollAnimation from "./scroll-animation";

export default function Trustpilot() {
  const trustbox = useRef(null);
  const trustboxCarousel = useRef(null);
  useEffect(() => {
    let aScript = document.createElement('script');
    aScript.type = 'text/javascript';
    aScript.src = "//widget.trustpilot.com/bootstrap/v5/tp.widget.bootstrap.min.js";
    aScript.async = "true";
    document.head.appendChild(aScript);
    aScript.onload = function () {
      window.Trustpilot.loadFromElement(trustbox.current);
      window.Trustpilot.loadFromElement(trustboxCarousel.current);
    };
  }, []);
  return (
    <ScrollAnimation className="trustpilot">
      <div id="fade-in-on-scroll">
        <div ref={trustbox} className="trustpilot-widget" data-locale="en-GB" data-template-id="53aa8807dec7e10d38f59f32"
             data-businessunit-id="4c96546500006400050de295" data-style-height="150px" data-style-width="100%"
             data-theme="light">
        </div>
        <div ref={trustboxCarousel}className="trustpilot-widget" data-locale="en-GB" data-template-id="54ad5defc6454f065c28af8b"
             data-businessunit-id="4c96546500006400050de295" data-style-height="220px" data-style-width="100%"
             data-theme="light" data-stars="5" data-schema-type="Organization">
        </div>
      </div>
    </ScrollAnimation>
  )
}
