import React from 'react';
import PropTypes from 'prop-types';

export default function TabButton({onClick, children, active}) {
  return (
    <button className={`tab-button ${active ? 'button--active' : ''}`} type="button" onClick={onClick}>
      {children}
    </button>
  )
}

TabButton.propTypes = {
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool,
};

TabButton.defaultProps = {
  active: false,
};
