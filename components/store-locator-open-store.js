import React, {Fragment, useCallback, useContext, useEffect} from "react";
import {StoreLocatorContext} from "../config/store-locator-context";
import {distance} from "../app/utils";
import CloseIcon from "./close-icon";
import DirectionsIcon from "./directions-icon";
import carriers from "../config/carriers";
const DAYS = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"];
export default function StoreLocatorOpenStore() {
  const {openStore, onClose, location, userLocation, filters, appEnv} = useContext(StoreLocatorContext);
  const handleKeyDown = useCallback(event => {
    if (event.key === "Escape") {
      onClose();
    }
  }, [onClose]);

  useEffect(() => {
    document.addEventListener("keydown", handleKeyDown);
    return () => {
      document.removeEventListener("keydown", handleKeyDown);
    };
  }, []);

  if (openStore === null) {
    return null;
  }
  const address = `${openStore.address}<br/>${openStore.city},<br/>${openStore.county ? openStore.county + ', <br/>' : ''}${openStore.postcode.toUpperCase()}`;
  const miles = distance(userLocation, {lat: openStore.latitude, lng: openStore.longitude});
  const storePhoto = openStore.photo_reference ? `https://maps.googleapis.com/maps/api/place/photo?key=${process.env.NEXT_PUBLIC_GA_PA_KEY}&photoreference=${openStore.photo_reference}&maxwidth=357` : `/find-a-store/map-location-default.jpg`;
  return (
    <div className="StoreLocatorOSM__open-store open">
      <div className="text-right">
        <button onClick={onClose}
                aria-label={`Close store information for ${openStore.site_name} and back to results list`}>
          <CloseIcon/>
        </button>
      </div>
      <hr/>
      <div className="flex flex-wrap StoreLocatorOSM__open-store-top">
        <div className="w-full sm:w-1/2">
          <a
            href={`https://www.google.com/maps/@?api=1&map_action=pano&viewpoint=${openStore.latitude},${openStore.longitude}`}
            target="_blank" className="street-view-link">
            <div className="street-view-link-img" style={{backgroundImage: `url(${storePhoto})`}}/>
          </a>
        </div>
        <div className="w-full sm:w-1/2 open-store-layout">
          <h3 className="StoreLocatorOSM__open-store-name"><strong>{openStore.site_name}</strong></h3>
          <p className="StoreLocatorOSM__open-store-address" dangerouslySetInnerHTML={{__html: address}}/>
          <p className="StoreLocatorOSM__open-store-closing-time">Open
            until {openStore[`${openStore.today}_close_formatted`]}</p>
          <p className="StoreLocatorOSM__open-store-distance">{miles} miles away</p>
          <a target="_blank"
             href={`https://www.google.com/maps/dir/Current+Location/${openStore.latitude},${openStore.longitude}`}
             className="StoreLocatorOSM__open-store-directions">
            Get directions
            <DirectionsIcon/>
          </a>
        </div>
      </div>
      <hr/>
      <div className="flex flex-wrap">
        <div className="w-full sm:w-1/2">
          <div className="StoreLocatorOSM__open-store-opening-times">
            <h3 className="StoreLocatorOSM__open-store-opening-times-title">Opening times:</h3>
            <ul>
              {DAYS.map((day, key) => {
                return (
                  <li key={key}>
                    {day === openStore.today ?
                      <Fragment key={`today_${day}`}>
                      <span>
                        <strong className="StoreLocatorOSM__open-store-opening-times-day">{day}</strong>
                      </span>
                        <span>
                        <strong>{openStore[`${day}_open_formatted`]} – {openStore[`${day}_close_formatted`]}</strong>
                      </span>
                      </Fragment> :
                      <Fragment key={`not_today_${day}`}>
                        <span className="StoreLocatorOSM__open-store-opening-times-day">{day}</span>
                        <span>{openStore[`${day}_open_formatted`]} – {openStore[`${day}_close_formatted`]}</span>
                      </Fragment>}
                  </li>
                );
              })}
            </ul>
          </div>
        </div>
        <div className="w-full sm:w-1/2">
          <div className="StoreLocatorOSM__open-store-carriers">
            <h3 className="StoreLocatorOSM__open-store-opening-times-title">Partners at this store:</h3>
            <ul className="StoreLocatorOSM__open-store-carriers-icons">
              {openStore.carriers && openStore.carriers.map((carrier, key) => {
                return<li key={key}><a href={carriers[carrier].link} target="_blank"><img src={carriers[carrier].img}/></a></li>
              })}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}
