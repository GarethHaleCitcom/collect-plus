import React, {useContext} from 'react';
import SwiperCore, {Navigation, Pagination} from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';
import {AppContext} from "../config/context";
import PropTypes from 'prop-types';

SwiperCore.use([Navigation, Pagination]);

export default function Feedback({className = '', id=''}) {
  const {feedback} = useContext(AppContext);
  const config = {
    spaceBetween: 0,
    slidesPerView: 1,
    navigation: {
      nextEl: '.button--next',
      prevEl: '.button--previous'
    },
    pagination: {
      el: '.feedback__carousel-pagination',
      clickable: true,
    }
  };
  return (
    <section className={`feedback ${className}`} id={id}>
      <div className="feedback__carousel">
        <div className="feedback__carousel-control">
          <img className="button--previous" src="/icons/chevron-yellow-left.svg"/>
        </div>
        <div className="feedback__carousel-wrapper">
          <Swiper
            {...config}>
            {
              feedback.map(({name, role, message, imageSrc}, key) => {
                return (
                  <SwiperSlide key={key}>
                    <div className="feedback__carousel__person">
                      {/*<div className="feedback__carousel__person__image person--desktop">*/}
                      {/*  <img className="feedback__carousel__person__image-image" src={imageSrc}/>*/}
                      {/*</div>*/}
                      <div className="feedback__carousel__person__content">
                        <p className="feedback__carousel__person__content-message">
                          {message}
                        </p>
                        <div className="feedback__carousel__person__content__person-info">
                          <div>
                            <p className="feedback__carousel__person__content__person-info-name">{name}</p>
                            {role && <p className="feedback__carousel__person__content__person-info-role">{role}</p>}
                          </div>
                          <img className="feedback__carousel__person__content__person-info-image person-image--mobile" src={imageSrc}/>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )
              })
            }
          </Swiper>
          <div className="feedback__carousel-pagination">

          </div>
        </div>
        <div className="feedback__carousel-control">
          <img className="button--next" src="/icons/chevron-yellow-right.svg"/>
        </div>
      </div>
    </section>
  )
}

Feedback.propTypes = {
  className: PropTypes.string
}
