import React, {useCallback, useContext} from "react";
import {StoreLocatorContext} from "../config/store-locator-context";
import SearchInput from "./search-input";
import PropTypes from 'prop-types';

export default function StoreLocatorSearch({title}) {
  const {location, onPostcodeSearch, postcode, setPostcode} = useContext(StoreLocatorContext);

  const handleSubmit = useCallback(event => {
    event.preventDefault();
    if (postcode.trim().length > 0) {
        onPostcodeSearch({postcode});
    }
  }, [postcode, onPostcodeSearch]);

  const handleChange = useCallback(event => {
    setPostcode(event.target.value.toUpperCase());
  }, [setPostcode]);

  if (location !== null) {
    return null;
  }

  return (
    <div className="StoreLocatorOSM__search">
      <div className="container-medium">
          <SearchInput
            placeholder="Enter your Town / Postcode"
            title={title}
            onSubmit={handleSubmit}
            onChange={handleChange}
            value={postcode}
          />
      </div>
    </div>
  );
}

StoreLocatorSearch.propTypes = {
  title: PropTypes.string,
};

StoreLocatorSearch.defaultProps = {
  title: "Find a Store",
};
