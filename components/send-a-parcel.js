import React, {useContext} from 'react';
import {AppContext} from "../config/context";
import Partners from "./partners";
import dynamic from "next/dist/next-server/lib/dynamic";

const StoreLocator = dynamic(() => import('../components/store-locator'), {ssr: false});

export default function SendAParcel() {
  return (
    <div className="flex flex-wrap">
      <Partners  title="Send a parcel from any Collect+ store across the UK via our partners"
                description="With PayPoint taking full ownership of the Collect+ brand, we are opening the Collect+ send service to all our courier partners:"/>
      <StoreLocator storeLocatorTitle="Send a Parcel"/>
    </div>
  )
}
