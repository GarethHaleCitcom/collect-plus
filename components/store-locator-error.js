import React, {useCallback, useContext} from "react";
import {StoreLocatorContext} from "../config/store-locator-context";

export default function StoreLocatorError() {
  const {errorMessage, setErrorMessage} = useContext(StoreLocatorContext);

  const handleClose = useCallback(() => {
    setErrorMessage(null);
  }, [errorMessage, setErrorMessage]);

  if (errorMessage === null) {
    return null;
  }

  return (
    <div className="StoreLocatorOSM__error">
      <div className="container-medium">
        <button onClick={handleClose}>&times;</button>
        {errorMessage}
      </div>
    </div>
  );
}
