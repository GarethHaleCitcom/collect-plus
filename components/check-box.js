import React from 'react';

export default function CheckBox(props) {
  return (
    <label className="check-box">{props.children}
      <input type="checkbox" name={props.name} checked={props.checked} onChange={props.onChange}/>
        <span className="checkmark"/>
    </label>
  )
}
