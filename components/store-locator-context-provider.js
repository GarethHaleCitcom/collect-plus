import React, {createRef, useCallback, useEffect, useMemo, useState} from "react";
import head from "lodash/head";
import {StoreLocatorContext} from "../config/store-locator-context";
import {fetchPostcodePosition, fetchStoresFromBounds, hasServices} from "../app/utils";
import config from "../config/store-locator";
import partners from "../config/partners";
const DEFAULT_FILTERS = {
  distance: 2.5,
  opening: null,
  carriers: [],
  services: {
    collect_plus: {
      name: "Collect+",
      selected: true,
    },
  },
};

export default function StoreLocatorContextProvider({children, filterServices = false}) {
  const map = createRef(null);
  const [fetching, setFetching] = useState(false);
  const [initialCenterLocation, setInitialCenterLocation] = useState(null);
  const [geoLocationSupported, setGeoLocationSupported] = useState(false);
  const [stores, setStores] = useState([]);
  const [highlightedStore, setHighlightedStore] = useState(null);
  const [openStore, setOpenStore] = useState(null);
  const [location, setLocation] = useState(null);
  const [userLocation, setUserLocation] = useState(null);
  const [postcode, setPostcode] = useState("");
  const [zoom, setZoom] = useState(config.initialZoom);
  const [filters, setFilters] = useState(JSON.parse(JSON.stringify(DEFAULT_FILTERS)));
  const [appliedFilters, setAppliesFilters] = useState(JSON.parse(JSON.stringify(DEFAULT_FILTERS)));
  const preAppliedServiceFilters = useMemo(() => !filterServices || !Object.keys(filterServices).length ? [] : filterServices.split(","), [filterServices]);
  const [errorMessage, setErrorMessage] = useState(null);

  const updateStores = async bounds => {
    const {ok, data} = await fetchStoresFromBounds(bounds) || {};
    ok && setStores(data.filter(store => hasServices(store, preAppliedServiceFilters)));
  }

  useEffect(() => {
    const geoLocationSupported = "navigator" in window;
    if (!geoLocationSupported) {
      setGeoLocationSupported(false);
      console.warn("No geo location support for this browser");
    }
    setGeoLocationSupported(geoLocationSupported);
  }, []);

  useEffect(() => {
    setFetching(false);
  }, [stores]);

  useEffect(() => {
    if (fetching || location === null) return;
    setFetching(true);
    const {_southWest, _northEast} = map.current.leafletElement.getBounds();
    const bounds = [[_southWest.lat, _southWest.lng], [_northEast.lat, _northEast.lng]];
    updateStores(bounds);
  }, [location]);

  const onMoveEnd = useCallback(event => {
    if (location !== null) {
      setLocation(event.target.getCenter());
    }
  }, [location, setLocation]);

  const onZoom = useCallback(event => {
    setZoom(event.target._zoom);
  }, [setZoom]);

  const onPostcodeSearch = useCallback(async ({postcode, position}) => {
    if (position) {
      setLocation(position);
      setUserLocation(position);
    }
    if (postcode) {
      setFetching(true);
      const {ok, data, errors} = await fetchPostcodePosition(postcode);
      setFetching(false);
      if (!ok || !data) {
        setErrorMessage(head(errors) || 'Location not found');
        return;
      }

      setErrorMessage(null);
      setLocation({lat: data.lat, lng: data.lng});
      setUserLocation({lat: data.lat, lng: data.lng});
    }
  }, [setLocation, setUserLocation, setFetching]);

  const onClose = useCallback(() => {
    setOpenStore(null);
  }, [setOpenStore]);

  const resetFilters = useCallback(() => {
    setFilters(JSON.parse(JSON.stringify(DEFAULT_FILTERS)));
    setAppliesFilters(JSON.parse(JSON.stringify(DEFAULT_FILTERS)));
  }, [setFilters, setAppliesFilters]);

  const applyFilters = useCallback(() => {
    setAppliesFilters(JSON.parse(JSON.stringify(filters)));
  }, [setAppliesFilters, filters]);

  const onHighlight = useCallback(store => {
    setHighlightedStore(store);
  }, [setHighlightedStore]);

  const onGeoLocation = useCallback(() => {
    if (!geoLocationSupported) {
      return;
    }
    navigator.geolocation.getCurrentPosition(({coords}) => {
      const position = {lat: coords.latitude, lng: coords.longitude};
    }, console.error); // @todo handle error
  }, [onPostcodeSearch]);

  return (
    <StoreLocatorContext.Provider value={{
      map,
      fetching,
      stores,
      openStore,
      highlightedStore,
      setHighlightedStore,
      location,
      zoom,
      onMoveEnd,
      onZoom,
      onPostcodeSearch,
      onClose,
      onHighlight,
      setOpenStore,
      onGeoLocation,
      geoLocationSupported,
      postcode,
      setPostcode,
      filters,
      setFilters,
      resetFilters,
      applyFilters,
      appliedFilters,
      setLocation,
      userLocation,
      errorMessage, setErrorMessage,
      initialCenterLocation,
    }}>
      {children}
    </StoreLocatorContext.Provider>
  );
}
