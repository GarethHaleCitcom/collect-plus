import React, {forwardRef, useContext, useEffect} from "react";
import Head from "next/head";
import {AppContext} from "../config/context";
import Error from "./error";
import Menu from "./menu";
import TopBar from "./top-bar";
import Footer from "./footer";
import CookieBanner from "./cookie-banner";

const GA_ID = !process.env.NEXT_PUBLIC_GA_ID ? "" : process.env.NEXT_PUBLIC_GA_ID;
const NODE_ENV = !process.env.NODE_ENV ? "development" : process.env.NODE_ENV;

export default forwardRef(function Screen(
  {name, description, children, meta = {}, error = null},
  ref
) {
  const {setScreenRef, analyticsEnabled} = useContext(AppContext);

  useEffect(() => {
    setScreenRef(ref);
  }, [ref]);

  return (
    <>
      <Head>
        {NODE_ENV === 'production' && analyticsEnabled && (
          <>
            <script
              async
              src={`https://www.googletagmanager.com/gtag/js?id=${GA_ID}`}
            />
            <script
              dangerouslySetInnerHTML={{
                __html: `
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', '${GA_ID}');`,
              }}
            />
          </>
        )}
        <title>{meta?.title ?? "Collect+"}</title>
        <link rel="icon" href="/favicon.ico"/>
        {description && <meta name="description" content={description}/>}
      </Head>
      <TopBar/>
      <Menu/>
      <div ref={ref}>
        <div className="screen-content">
          {error !== null ? <Error message={error}/> : children}
        </div>
      </div>
      <Footer/>
      <CookieBanner/>
    </>
  );
});
