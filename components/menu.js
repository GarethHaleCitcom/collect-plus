import React, {useContext} from 'react';
import Link from "next/link";
import {AppContext} from "../config/context";

export default function Menu() {
    const {menu, isMenuLinkActive} = useContext(AppContext);
    return (
        <nav className="menu">
            <div className="container-medium">
                <ul className="menu__list">
                    {
                        menu.map(({path, name, href, external}, key) => {
                            return (
                                <li key={key} className={`menu__list__item${isMenuLinkActive(path) ? ' item--active' : ''}`}>
                                  {
                                    external ? <a className="menu__list__item-link" href={href}>
                                      {name}
                                    </a> : <Link className="menu__list__item-link" key={key} href={path}>{name}</Link>
                                  }
                                </li>
                            )
                        })
                    }
                </ul>
            </div>
        </nav>
    )
}
