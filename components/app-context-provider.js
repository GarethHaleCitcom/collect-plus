import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import {AppContext} from "../config/context";
import menu from "../config/menu";
import feedback from "../config/feedback";
import blogs from "../config/blogs";
import partners from "../config/partners";
import footer from "../config/footer";
import reviews from "../config/reviews";
import parcelReturn from '../config/parcel-return';
import blogCategories from '../config/blog-categories';
import helpAndAdvice from '../config/help-and-advice';

export default function AppContextProvider({ children }) {
    const { asPath, pathname} = useRouter();
    const [menuOpen, setMenuOpen] = useState(false);
    const [screenRef, setScreenRef] = useState(null);
    const [appEnv, setAppEnv] = useState({});
    const [analyticsEnabled, setAnalyticsEnabled] = useState(false);
    const [serviceMessageClosed, setServiceMessageClosed] = useState(false);
    const isMenuLinkActive = (path) => {
        return pathname === path;
    };

    useEffect(() => {
        setMenuOpen(false);
        if (screenRef && screenRef.current) {
            screenRef.current.scrollTop = 0;
        }
    }, [asPath]);
    const findBlogPost = (slug) => {
      return blogs.find((blog) => {
        return blog.data.slug === slug;
      })
    };
    const context = {
      menuOpen,
      setMenuOpen,
      setScreenRef,
      menu,
      isMenuLinkActive,
      appEnv,
      reviews,
      feedback,
      blogs,
      partners,
      footer,
      findBlogPost,
      parcelReturn,
      blogCategories,
      helpAndAdvice,
      analyticsEnabled,
      setAnalyticsEnabled,
      serviceMessageClosed,
      setServiceMessageClosed
    };

    return (
        <AppContext.Provider value={context}>
            {children}
        </AppContext.Provider>
    );
}
