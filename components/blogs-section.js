import React, {useState, useContext} from 'react';
import PropTypes from 'prop-types';
import ScrollAnimation from "./scroll-animation";

export default function BlogsSection({title, children}) {
  return (
    <ScrollAnimation className="blogs-section">
      <div className="blogs-section__title">
        <h1 className="blogs-section__title-text">{title}</h1>
      </div>
      <div className="flex flex-wrap">
        {children}
      </div>
    </ScrollAnimation>
  )
}

BlogsSection.propTypes = {
  title: PropTypes.string
};

BlogsSection.defaultProps = {
  title: 'Latest Blogs'
};
