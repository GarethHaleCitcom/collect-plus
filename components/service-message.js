import React, {useState, useCallback, useContext, useEffect} from 'react';
import PropTypes from 'prop-types';
import StyledLink from "./styled-link";
import Cookie from "../services/Cookie";
import {isDOMAvailable} from '../app/helpers';
import {AppContext} from "../config/context";

export default function ServiceMessage() {
  const {serviceMessageClosed, setServiceMessageClosed} = useContext(AppContext);
  const handleCloseButton = useCallback(() => {
    // Cookie.set('service_message', {closed: false});
    setServiceMessageClosed(true);
  }, [setServiceMessageClosed]);
  // useEffect(() => {
  //   if(isDOMAvailable()) {
  //     const cookie = Cookie.get('service_message');
  //     if(!cookie) {
  //       setClosed(false);
  //     }
  //   }
  // }, []);

  if (serviceMessageClosed) {
    return <div></div>
  }

  return (
    <div className="service-message">
      <div className="container-medium">
        <div className="service-message__content">
          <div className="service-message__content-wrapper">
            <span>Service Message: </span>
            <span className="service-message__content-message"> Earlier this year PayPoint acquired Yodel’s 50% stake in Collect+ making it the sole owner of the UK’s largest parcel shop network working with a range of partners: Amazon, DHL, eBay, FedEx, Parcel2Go and Yodel. Any parcel queries from before 1 October should be referred to <a target="_blank" href="https://www.yodel.co.uk/help-centre">Yodel</a>.</span>
          </div>
          <div className="service-message__content__actions">
            <img className="service-message__content__actions-close" src="/icons/close-icon.svg"
                 onClick={handleCloseButton}/>
          </div>
        </div>
      </div>
    </div>
  )
}

