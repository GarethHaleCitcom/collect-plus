import React, {Fragment, useCallback, useContext, useEffect, useMemo} from "react";
import {StoreLocatorContext} from "../config/store-locator-context";
import {filterStores} from "../app/utils";
import {app, isDOMAvailable} from "../app/helpers";

let L = null;
let markerIcon = null;
let markerUserLocation = null;
let Marker = null;

if(isDOMAvailable()) {
    L = require('leaflet');
    Marker = require('react-leaflet').Marker;
    markerIcon = L.icon({iconUrl: '/icons/pin-collect-plus.svg', iconSize: [74, 58]});
    markerUserLocation = L.icon({iconUrl: '/icons/pin-user-location.png', iconSize: [27, 42]});
}

export default function StoreLocatorStores() {
    const {setOpenStore, stores, highlightedStore, onHighlight, location, userLocation, appliedFilters} = useContext(StoreLocatorContext);
    let filteredStores = useMemo(() => filterStores(stores, location, appliedFilters), [stores, location, appliedFilters]);

    const handleClick = useCallback(store => {
        return () => setOpenStore(store);
    }, [setOpenStore]);

    const handleMouseOver = useCallback(store => {
        return () => onHighlight(store);
    }, [onHighlight]);

    const handleMouseOut = useCallback(() => {
        onHighlight(null);
    }, [onHighlight]);

    return (
        <Fragment>
          {userLocation && (
            <Marker
              icon={markerUserLocation}
              position={[userLocation.lat, userLocation.lng]}
            />
          )}
          {filteredStores.map(store => {
            let opacity = 1;
            if (highlightedStore !== null) {
              opacity = store.id === highlightedStore.id ? 1 : 0.3;
            }
            return (
              <Marker
                key={store.id}
                icon={ markerIcon}
                position={[store.latitude, store.longitude]}
                onClick={handleClick(store)}
                onMouseOver={handleMouseOver(store)}
                onMouseOut={handleMouseOut}
                opacity={opacity}
              />
            );
          })}
        </Fragment>
    );
}
