import React from 'react';
import SearchIcon from "./search-icon";

export default function SearchInput({onSubmit, onChange, errorMessage, title, value, placeholder, text}) {
  const handleSubmit = (e) => {
    onSubmit && onSubmit(e);
  };
  const handleChange = (e) => {
    onChange && onChange(e);
  };
  return (
    <div className="search-input">
      <img className="search-input-graphic" src="/graphics/header-orange-graphic.svg"/>
      <form className="search-input__form" onSubmit={handleSubmit}>
        <h1 className="search-input__form-title">{title}</h1>
        <div className="search-input__form-wrapper">
          <input placeholder={placeholder} value={value} required onChange={handleChange}/>
          <button type="submit" className="search-input__form-button">
            <SearchIcon/>
          </button>
        </div>
        {text && <p className="search-input__form-description">{text}</p>}
        {errorMessage && <span className="search-input__form-error">{errorMessage}</span>}
      </form>
    </div>
  )
}
