import React, {useState, useRef, useEffect, useContext, createRef} from 'react';
import Link from "next/link";
import {AppContext} from "../config/context";

export default function MobileMenu() {
    const menuList = useRef();
    const {menu, isMenuLinkActive, menuOpen, setMenuOpen} = useContext(AppContext);
    const [topBarHeight, setTopBarHeight] = useState(0);
    const mobileMenuPlaceholder = useRef(null);
    const [transitioning, setTransitioning] = useState(false);
    const topBar = createRef();
    let animId = null;

    const collapseMenu = () => {
        let height = 0;
        if(menuOpen) {
            height = window.innerHeight - topBarHeight;
        }
        animId = requestAnimationFrame(() => {
            (menuList.current.style.height = `${height}px`);
            cancelAnimationFrame(animId);
        });
    };

    useEffect(() => {
      if(!menuList.current) {return}
      menuList.current.addEventListener('transitionstart', () => {

        setTransitioning(true);
      });
      menuList.current.addEventListener('transitionend', () => {
        setTransitioning(false)
      })
    }, []);

    useEffect(() => {
        if(topBar.current) {
            setTopBarHeight(topBar.current.offsetHeight);
        }
    }, [topBar])

    useEffect(() => {
        menuList.current !== '' && collapseMenu();
    }, [menuOpen]);

    return (
        <div className={`mobile-menu${menuOpen ? ' menu--open' : ''} ${transitioning ? 'menu--transitioning' : ''}`}>
            <div className="mobile-menu__top" ref={topBar}>
                <div className="mobile-menu__top__logo">
                    <img className="mobile-menu__top__logo-img" src="/logo.svg"/>
                </div>
                <div className={`mobile-menu__top__burger-button${menuOpen ? ' open' : ''}`} onClick={() => setMenuOpen(!menuOpen)}>
                    <span/>
                    <span/>
                    <span/>
                    <span/>
                    <span/>
                    <span/>
                </div>
            </div>
            <ul ref={menuList} className="mobile-menu__list">
                {
                    menu.map(({path, name}, key) => {
                        return (
                            <li key={key} className={`mobile-menu__list__item${isMenuLinkActive(path) ? ' item--active' : ''}`}>
                                <Link className="mobile-menu__list__item-link" key={key} href={path}>{name}</Link>
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    )
}
