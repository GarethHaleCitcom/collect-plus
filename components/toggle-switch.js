import React from 'react';
export default function ToggleSwitch(props) {

  return (
    <label className="Switch__Input">
      <input type="checkbox" {...props}/>
      <span className="slider round"></span>
    </label>
  )
}
