import React, {useState, useEffect, useContext} from 'react';
import PropTypes from 'prop-types';

import StyledLink from "./styled-link";
import CloseIcon from "./close-icon";
import {AppContext} from "../config/context";
import parcelClients from '../config/parcel-clients';
export default function TrackOverlay ({linkHref, clientId, title, onClosePressed}) {
  const {partners} = useContext(AppContext);
  const [defaultDescription, setDefaultDescription] = useState(null);
  const defaultMessage = "We've got no information about your parcel.";

  useEffect(() => {
    if(title === '') {
      setDefaultDescription("For more information on your parcel, please visit the partner below:");
    }
    else if(title !== 'Parcel awaiting collection') {
      setDefaultDescription('Please contact the relevant partner below:');
    }
  }, []);
  return (
    <div className="track-overlay">
      <button className="track-overlay-back" onClick={onClosePressed}>
        <CloseIcon/>
      </button>
      <div className="track-overlay__content">
        <h2 className="track-overlay__content-title">{title === "" ? defaultMessage : title}</h2>
        {title === "" && <p className="track-overlay__content-description">
          {defaultDescription}
        </p>}
        {
          title !== 'Parcel awaiting collection' && title !== '' &&
            <ul>
              {
                partners.map(({link, name}) => {
                  return (
                    <li><a target="_blank" href={link}>{name}</a></li>
                  )
                })
              }
            </ul>
        }
        {clientId && <img className="track-overlay__content-image" src={parcelClients[clientId]}/>}
      </div>
      <div className="track-overlay__link">
        {title !== 'Parcel awaiting collection' &&
        <StyledLink href={linkHref} internalLink={true}>
          View parcel
        </StyledLink>}
      </div>
    </div>
  )
}

TrackOverlay.propTypes = {
  title: PropTypes.string,
  linkHref: PropTypes.string.isRequired,
};

TrackOverlay.defaultProps = {
  title: 'Title here',
};
