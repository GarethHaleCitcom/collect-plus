import React, {useContext, useState} from "react";
import PropTypes from 'prop-types'
import dynamic from "next/dynamic";
import {Map, TileLayer, ZoomControl} from "react-leaflet";
import config from "../config/store-locator";
import StoreLocatorSearch from "./store-locator-search";
import StoreLocatorStores from "./store-locator-stores";
import StoreLocatorSidebar from "./store-locator-sidebar";
import StoreLocatorOpenStore from "./store-locator-open-store";
import StoreLocatorContextProvider from "./store-locator-context-provider";
import {StoreLocatorContext} from "../config/store-locator-context";
import TabButton from "./tab-button";
import ParcelReturn from "./parcel-return";
import StoreLocatorError from "./store-locator-error";

const Track = dynamic(() => import('./track'), {ssr: false});

function StoreLocatorComponent({onlyStoreLocator, storeLocatorTitle,  defaultActive = 'find'}) {
  const {map, zoom, location, onMoveEnd, onZoom, errorMessage} = useContext(StoreLocatorContext);
  const [activeTab, setActiveTab] = useState(defaultActive);
  const handleTabClick = tabId => () => {
    setActiveTab(tabId);
  };

  return (
    <>
      {!onlyStoreLocator &&
      <div className="container-medium">
        <div className="StoreLocatorOSM-tabs">
          <TabButton onClick={handleTabClick('find')} active={activeTab === 'find'}>
            Find
          </TabButton>
          <TabButton onClick={handleTabClick('track')} active={activeTab === 'track'}>
            Track
          </TabButton>
          <TabButton onClick={handleTabClick('return')} active={activeTab === 'return'}>
            Return
          </TabButton>
        </div>
      </div>}
      <div id="store-locator" className={`StoreLocatorOSM ${onlyStoreLocator ? "border--gray" : 'border--black'}`} id="store-locator">
        {activeTab === 'find' && (
          <div className="StoreLocatorOSM__map">
            {Map && (
              <Map
                center={!location || !location.lng ? config.startLocation : location}
                onMoveEnd={onMoveEnd}
                onZoom={onZoom}
                dragging={location !== null}
                attributionControl={false}
                zoom={zoom}
                minZoom={config.minZoom}
                maxZoom={config.maxZoom}
                zoomControl={false}
                scrollWheelZoom={false}
                ref={map}
              >
                <TileLayer
                  attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                  url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                />
                <ZoomControl position="bottomright"/>
                {location !== null && location.lng && <StoreLocatorStores/>}
              </Map>
            )}
            {errorMessage === null && (
              <>
                <StoreLocatorSearch title={storeLocatorTitle || 'Find a Store'}/>
                <StoreLocatorSidebar/>
                <StoreLocatorOpenStore/>
              </>
            )}
            <StoreLocatorError/>
          </div>
        )}

        {activeTab === 'track' && <Track/>}
        {activeTab === 'return' && <ParcelReturn hasLink={true}/>}
      </div>
    </>
  )
}

export default function StoreLocator({onlyStoreLocator, storeLocatorTitle}) {
  return (
    <StoreLocatorContextProvider>
      <StoreLocatorComponent onlyStoreLocator={onlyStoreLocator} storeLocatorTitle={storeLocatorTitle}/>
    </StoreLocatorContextProvider>
  );
}

StoreLocator.propTypes = {
  onlyStoreLocator: PropTypes.bool,
  storeLocatorTitle: PropTypes.string
};
StoreLocator.defaultProps = {
  onlyStoreLocator: true,
  storeLocatorTitle: null
};
