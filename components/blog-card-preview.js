import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import StyledLink from "./styled-link";
import Link from "next/link";

export default function BlogCardPreview({title, description, slug, imageSrc, linkText}) {
  const maxCharacters = 80;
  const [text, setText] = useState('');
  useEffect(() => {
    if(description.length > maxCharacters) {
      setText(`${description.substring(0, maxCharacters)}...`);
    } else {
      setText(description);
    }
  }, []);
  return (
    <div id="fade-in-on-scroll" className="w-full sm:w-1/2 md:w-1/3 blog-card-preview--layout">
      <div className="blog-card-preview">
        <Link href={`/blogs/${slug}`}>
          <div className="blog-card-preview__image">
            <div className="blog-card-preview__image-image" style={{backgroundImage: `url(${imageSrc})`}}/>
          </div>
        </Link>
        <div className="blog-card-preview__content">
          <h3 className="blog-card-preview__content-title">{title}</h3>
          <p className="blog-card-preview__content-description">{text}</p>
        </div>
        <div className="blog-card-preview__link">
          <StyledLink href={`/blogs/${slug}`} styleType="rounded-gray">
            {linkText}
          </StyledLink>
        </div>
      </div>
    </div>
  )
}

BlogCardPreview.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  imageSrc: PropTypes.string.isRequired,
  linkText: PropTypes.string,
};

BlogCardPreview.defaultProps = {
  linkText: 'Read More'
};
