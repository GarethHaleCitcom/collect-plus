import React, {useCallback, useContext, useMemo, useEffect, useRef, useState} from "react";
import get from "lodash/get";
import {distance, filterStores} from "../app/utils";
import {StoreLocatorContext} from "../config/store-locator-context";
import SliderInput from "./slider-input";
import SearchIcon from "./search-icon";
import CloseIcon from "./close-icon";
import DirectionsIcon from "./directions-icon";
import InfoIcon from "./info-icon";
import CheckBox from "./check-box";
import partners from "../config/partners";
import clone from 'lodash/clone';

export default function StoreLocatorSidebar() {
  const scrollContainer = useRef(null);
  const {onPostcodeSearch, userLocation, setOpenStore, openStore, stores, onHighlight, highlightedStore, geoLocationSupported, onGeoLocation, postcode, setPostcode, filters, setFilters, resetFilters, applyFilters, appliedFilters, setLocation} = useContext(StoreLocatorContext);
  const [open, setOpen] = useState(true);
  const [showFilters, setShowFilters] = useState(false);
  const [sortedAndFilteredStores, setSortedAndFilteredStores] = useState([]);
  const sort = useCallback((a, b) => {
    const aD = distance(userLocation, {lat: a.latitude, lng: a.longitude});
    const bD = distance(userLocation, {lat: b.latitude, lng: b.longitude});
    return aD - bD;
  }, [userLocation]);
  useEffect(() => {
    if(sortedAndFilteredStores.length > 0) {return}
    setSortedAndFilteredStores(filterStores(stores, userLocation, appliedFilters).sort(sort));
  }, [userLocation, stores, sort, appliedFilters]);
  const toggleOpen = useCallback(() => {
    let containerHeight = 0;
    setOpen(open => !open);
    if (!open) {
      scrollContainer.current.style['max-height'] = 'none';
      scrollContainer.current.style.maxHeight = '0px';
      containerHeight = 300;
      // containerHeight = scrollContainer.current.clientHeight;
    }
    requestAnimationFrame(() => {
      scrollContainer.current.style.maxHeight = `${containerHeight}px`;
    });
  }, [setOpen, open]);

  const toggleFilters = useCallback(() => {
    setShowFilters(showFilters => !showFilters);
  }, [setShowFilters]);

  const handleSubmit = useCallback(event => {
    event.preventDefault();
    if (postcode.trim().length > 0) {
      onPostcodeSearch({postcode});
    }
  }, [postcode, onPostcodeSearch]);

  const handleChange = useCallback(event => {
    setPostcode(event.target.value.toUpperCase());
  }, [setPostcode]);

  const handleClick = useCallback(store => {
    return () => setOpenStore(store);
  }, [setOpenStore]);

  const handleMouseOver = useCallback(store => {
    return () => onHighlight(store);
  }, [onHighlight]);

  const handleMouseOut = useCallback(() => {
    onHighlight(null);
  }, [onHighlight]);

  const updateDistance = useCallback((distance) => {
    setFilters(filters => {
      filters.distance = distance;
      return {...filters};
    });
  }, [setFilters]);

  const toggleOpening = useCallback((opening) => {

    setFilters(filters => {
      filters.opening = opening;
      return {...filters};
    });
  }, [setFilters]);

  const handlePartnersFilterChange = useCallback((partner) => {
    if(!filters.carriers.includes(partner)) {
      setFilters((filters) => {
        filters.carriers = [...filters.carriers, partner];
        return {...filters}
      });
    } else {
      const filtered = clone(filters.carriers);
      filtered.splice(filters.carriers.indexOf(partner), 1);
      setFilters((filters) => {
        filters.carriers = filtered;
        return {...filters}
      });
    }
  }, [filters.carriers]);

  if (userLocation === null || openStore !== null) {
    return null;
  }
  const highlightedStoreId = get(highlightedStore, "id");
  const distanceRange = [];
  let n = 0.1;

  while (n <= 5) {
    distanceRange.push(n);
    n = parseFloat((n + 0.1).toFixed(1));
  }
  return (
    <div className={`StoreLocatorOSM__sidebar ${!open && !showFilters ? "closed" : ""}`}>
      <div className="sidebar-nav">
        <button type="button" onClick={() => setLocation(null)}>
          <CloseIcon/>
        </button>
      </div>
      <form className="box" onSubmit={handleSubmit}>
        <div className="search">
          <div className="box__search-input-wrapper">
            <input placeholder="Enter your Town / Postcode" value={postcode} onChange={handleChange}/>
            <div className="box-search-icon">
              <SearchIcon className="icon--white"/>
            </div>
          </div>
          <button type="submit" className="button button-search">
            Search
          </button>
        </div>
        {geoLocationSupported && <button type="button" className="button-position" onClick={onGeoLocation}/>}
      </form>
      <div className="popupControls">
        {!showFilters && (
          <button type="button" className="hideResultsButton"
                  onClick={toggleOpen}>
            {open ? "Hide " : "Show "}
            results
          </button>
        )}
        <button type="button" className="filterButton"
                onClick={toggleFilters}>{showFilters ? "Back to " : "Filter"} results
        </button>
      </div>
      {!showFilters && (
        <div className="results" ref={scrollContainer}>
          {sortedAndFilteredStores.map((store, index) => {
            const miles = distance(userLocation, {lat: store.latitude, lng: store.longitude});
            return (
              <div
                key={store.id}
                className={`result ${highlightedStoreId === store.id ? "result--highlight" : ""}`}
                onClick={handleClick(store)}
                onMouseOver={handleMouseOver(store)}
                onMouseOut={handleMouseOut}
              >
                <div className="counter">{index + 1}</div>
                <div className="info">
                  <strong>{store.site_name}</strong><br/>
                  Open until {store[`${store.today}_close_formatted`]}<br/>
                  {miles} mile{miles !== 1 ? 's' : ''} away
                </div>
                <div className="buttons">
                  <button type="button" className="button button-info">
                    <InfoIcon/>
                  </button>
                  <a target="_blank"
                     onClick={event => event.stopPropagation()}
                     href={`https://www.google.com/maps/dir/Current+Location/${store.latitude},${store.longitude}`}
                     className="button button-directions"
                  >
                    <DirectionsIcon/>
                  </a>
                </div>
              </div>
            );
          })}
        </div>
      )}
      {showFilters && (
        <div className="filters">
          <h2 className="filters-title filters--distance">Distance (miles)</h2>
          <SliderInput
            className="filterDistance"
            name="distance"
            value={filters.distance}
            tabIndex={2}
            options={distanceRange}
            onChange={updateDistance}
            step={0.1}
          />
          <h2 className="filters-title">Opening hours</h2>
          <div className="filterOpeningHoursButtons">
            <CheckBox name="opening-now" onChange={() => {
              if (filters.opening === 'now') {
                return toggleOpening("");
              }
              toggleOpening("now");
            }} checked={filters.opening === 'now'}>
              Open now
            </CheckBox>
            <CheckBox name="opening-now" onChange={() => {
              if (filters.opening === 'allDay') {
                return toggleOpening("");
              }
              toggleOpening("allDay");
            }} checked={filters.opening === 'allDay'}>
              Open 24/7
            </CheckBox>
          </div>
          <div className="filters-partners">
            <h2 className="filters-title">Filter by partner:</h2>
            {
              partners.map(({name, id}, key) => {
                return (
                    <CheckBox key={key} name={id} onChange={() => handlePartnersFilterChange(id)} checked={filters.carriers.includes(id)}>
                      <span>{name}</span>
                    </CheckBox>
                )
              })
            }
          </div>
          <div className="filters__actions">
            <button type="button" className="filters__actions-action" onClick={resetFilters}>Clear</button>
            <button type="button" className="filters__actions-action" onClick={applyFilters}>Apply</button>
          </div>
        </div>
      )}
    </div>
  );
}
