import React, {useContext} from 'react';
import {AppContext} from "../config/context";
import Link from "next/link";

export default function Footer() {
  const {footer} = useContext(AppContext);

  return (
    <footer className="footer">
      <div className="container-medium">
        <div className="flex flex-wrap-reverse">
          <div className="w-full sm:w-4/5">
            <div className="footer__links">
              <div className="flex flex-wrap justify-between sm:justify-start">
                {
                  footer.links.map((items, key) => {
                    return (
                      <div key={`footer__links__list__${key}`}
                           className="w-1/3 sm:w-1/2 md:w-1/3">
                        <ul className="footer__links-list"
                        >
                          {
                            items.map(({to, name}, key) => {
                              return (
                                <li className="footer__links-list-item" key={`footer__links__list__item__${key}`}>
                                  <Link href={to}>
                                    {name}
                                  </Link>
                                </li>
                              )
                            })
                          }
                        </ul>
                      </div>
                    )
                  })
                }
              </div>
            </div>
          </div>
          <div className="w-full sm:w-1/5">
            <div className="footer-social">
              {
                footer.social.map(({href, component}, key) => {
                  let Icon = component;
                  return (
                    <a className="footer-social-icon" key={key} href={href} target="_blank">
                      <Icon/>
                    </a>
                  )
                })
              }
            </div>
          </div>
        </div>
        <span className="footer-separator"/>
        <div className="flex flex-wrap-reverse footer-content-row">
          <div className="w-full sm:w-4/5">
            <ul>
              {
                footer.bottomLinks && footer.bottomLinks.map(({name, to}, key) => {
                  return (
                    <li key={key}>
                      <Link href={to}><span>{name}</span></Link>
                    </li>
                  )
                })
              }
              <li>
                <Link href="/sitemap">
                  Sitemap
                </Link>
              </li>
            </ul>
          </div>
          <div className="w-full sm:w-1/5">
            <div className="footer-logo">
              <Link href="/">
                <img src={footer.logoSrc}/>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}
