import React, {useContext} from 'react';
import {AppContext} from "../config/context";
import StyledLink from "./styled-link";
import PropTypes from 'prop-types';

export default function ParcelReturn({hasLink, hasBorder}) {
  const {parcelReturn} = useContext(AppContext);
  return (
    <div className={`parcel-return ${hasBorder ? 'with--border' : ''}`}>
      <div className="container-medium">
        <h1 className="parcel-return-title">
          How it works
        </h1>
        <div className="flex flex-wrap">
          {
            parcelReturn && parcelReturn.map(({description, imageSrc, id}, key) => {
              return (
                <div key={key} className="w-full sm:w-1/2 md:w-1/3 card--layout">
                  <div className="parcel-return__card">
                    <div className="parcel-return__card-image-wrapper">
                      <img className="parcel-return__card-image" src={imageSrc}/>
                      <span className="parcel-return__card-image-counter">{id}</span>
                    </div>
                    <p className="parcel-return__card-description" dangerouslySetInnerHTML={{__html: description}}/>
                  </div>
                </div>
              )
            })
          }
        </div>
        {/*{*/}
        {/*  hasLink && (*/}
        {/*    <div className="parcel-return__link">*/}
        {/*      <StyledLink styleType="orange-pill" href="/parcel-returns">*/}
        {/*        Return*/}
        {/*      </StyledLink>*/}
        {/*    </div>*/}
        {/*  )*/}
        {/*}*/}
      </div>
    </div>
  )
}

ParcelReturn.propTypes = {
  hasLink: PropTypes.bool,
};

ParcelReturn.defaultProps = {
  hasLink: false,
};
