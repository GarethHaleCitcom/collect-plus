import React, {useContext} from 'react';
import {AppContext} from "../config/context";
import ScrollAnimation from "./scroll-animation";
import PropTypes from 'prop-types';
import sendAParcel from '../config/send-a-parcel';

const Partner = ({asLink, link, imageSrc}) => {
  return (
    <a href={link} target="_blank" id="fade-in-on-scroll">
      <img src={imageSrc}/>
    </a>
  )
};

export default function Partners({asLinks, title, sendLinks, styleType, description}) {
  const {partners} = useContext(AppContext);
  const items = sendLinks ? sendAParcel : partners;
  return (
    <ScrollAnimation className="partners">
      <h1 id="fade-in-on-scroll" className={`partners-title ${styleType}`}>
        {title}
      </h1>
      {description && <p className={`partners-description ${styleType.includes('title--orange') ? 'description--gray' : ''}`} dangerouslySetInnerHTML={{__html: description}}/>}
      <div className="flex flex-wrap justify-center">
        {
          items.map(({imageSrc, link, sendLink}, key) => {
            return (
              <div id="fade-in-on-scroll" key={key} className="w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/6 partners-partner">
                <Partner imageSrc={imageSrc} asLink={asLinks} link={sendLinks ? sendLink : link}/>
              </div>
            )
          })
        }
      </div>
    </ScrollAnimation>
  );
}

Partners.propTypes = {
  asLink: PropTypes.bool,
  title: PropTypes.string,
  styleType: PropTypes.string,
  description: PropTypes.string,
  sendLinks: PropTypes.bool,
};
Partners.defaultProps = {
  asLink: true,
  title: 'Who we work with',
  styleType: 'title--with-border',
  description: 'Collect+ are proud to work with some of the leading parcel couriers/retailers. Below is a list of our partners:',
  sendLinks: false
};

