import React, {useState, useEffect} from "react";
import PropTypes from "prop-types";
import Slider, {Handle} from "rc-slider";

export default function SliderInput(props) {
  const [state, setState] = useState({min: 0, max: 1});

  useEffect(() => {
    const {options} = props;
    const min = options[0];
    const max = options[options.length - 1];

    setState({min, max});
  }, []);

  const handle = handleProps => {
    const {value, ...restProps} = handleProps;
    restProps.dragging = String(restProps.dragging);

    return <Handle {...restProps} value={value}
                   data-value={value}/>;
  };
  const {min, max} = state;
  return (
    <Slider
      {...props}
      min={min}
      max={max}
      handle={handle}
    />
  )
}

SliderInput.propTypes = {
  options: PropTypes.array.isRequired
};
