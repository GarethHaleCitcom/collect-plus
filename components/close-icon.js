import React from 'react';
import PropTypes from 'prop-types';

export default function CloseIcon({className}) {

  return (
    <svg version="1.1"
         className={`close-icon ${className}`}
         xmlns="http://www.w3.org/2000/svg"
         x="0px"
         y="0px"
         viewBox="0 0 40 40">
      <path className="close-icon-path" d="M20,0C9,0,0,9,0,20s9,20,20,20s20-9,20-20S31,0,20,0z M25.1,27.2L20,22.2
	l-5.1,5.1c-0.6,0.6-1.6,0.6-2.2,0.1c-0.6-0.6-0.6-1.6-0.1-2.2c0,0,0,0,0.1-0.1l5.1-5.1l-5.1-5.1c-0.6-0.6-0.6-1.6-0.1-2.2
	c0.6-0.6,1.6-0.6,2.2-0.1c0,0,0,0,0.1,0.1l5.1,5.1l5.1-5.1c0.6-0.6,1.6-0.6,2.2-0.1c0.6,0.6,0.6,1.6,0.1,2.2c0,0,0,0-0.1,0.1
	L22.2,20l5.1,5.1c0.6,0.6,0.6,1.6,0,2.2C26.6,27.8,25.7,27.8,25.1,27.2C25.1,27.2,25.1,27.2,25.1,27.2z"/>
    </svg>
  )
}

CloseIcon.propTypes = {
  className: PropTypes.string,
};

CloseIcon.defaultProps = {
  className: 'icon--default',
}

