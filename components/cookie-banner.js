import React, {useState, useCallback, useEffect, useContext} from 'react';
import ToggleSwitch from "./toggle-switch";
import Cookie from "../services/Cookie";
import StyledLink from "./styled-link";
import Link from "next/link";
import {AppContext} from "../config/context";
import Button from "./button";

const DEFAULT_DATA = {
  strictly_necessary: true,
  performance: false,
};
const COOKIE_NAME = "cookies_settings";

export default function CookieBanner(props) {
  const [displayOverlay, setDisplayOverlay] = useState(false);
  const [displayBanner, setDisplayBanner] = useState(false);
  const {setAnalyticsEnabled} = useContext(AppContext);
  const [data, setData] = useState(DEFAULT_DATA);
  const [cookieDetail, setCookieDetail] = useState(false);

  const handleAcceptAll = useCallback(() => {
    const data = {
      strictly_necessary: true,
      performance: true,
    };
    Cookie.set(COOKIE_NAME, data, 182.5);
    setData(data);
    setDisplayBanner(false);
    setDisplayOverlay(false);
  }, [data, setData]);
  const handleSubmit = useCallback(() => {
    Cookie.set(COOKIE_NAME, data, 182.5);
    setDisplayBanner(false);
    setDisplayOverlay(false);
  }, [data, setData]);

  const handleChange = useCallback((e) => {
    setData({...data, [e.target.name]: e.target.checked});
  }, [data, setData]);

  const setOverlay = useCallback(() => {
    setDisplayOverlay(true);
  }, [setDisplayOverlay]);

  const toggleCookiePannel = useCallback(() => {
    setDisplayOverlay(true);
  }, [displayBanner, displayOverlay]);
  useEffect(() => {
    const cookieDetail = Cookie.get(COOKIE_NAME);
    if (!cookieDetail) {
      return setDisplayBanner(true);
    }
    if (cookieDetail.performance) {
      setAnalyticsEnabled(true);
    }
  }, []);
  if (displayOverlay) {
    return (
      <>
        <div className="cookie-banner-detail">
          <p>
            We use strictly_necessary cookies to make our site work. We would also like to use optional performance cookies to
            help
            us improve your use of the site and the services it offers. We will not set the optional cookies if you tell
            us you do not want us to enable them.
          </p>
          <form onSubmit={handleSubmit}>
            <span className="cookie-banner-detail-link">Strictly Necessary Cookies</span>
            <ToggleSwitch name="strictly_necessary" onChange={handleChange} checked={true}/>
            <p>
              These cookies enable core functionality of the website. Without them we will not be able to provide you with the services you wish to use.
            </p>
            <span className="cookie-banner-detail-link">Performance Cookies</span>
            <ToggleSwitch name="performance" onChange={handleChange} checked={data.performance}/>
            <p>
              These cookies help us monitor the website and make improvements by collecting and reporting information on
              how you use it. These cookies collect information such as number of visitors or what pages are used most
              often, but do not collect information in a way that directly identifies anyone. These include Google
              Analytics cookies.
            </p>
            <p>
              You can visit our <Link href="/privacy-policy"><span>Cookies Policy</span></Link> for more information
              about the cookies we use and our <Link href="/privacy-policy"><span>Privacy Notice</span></Link> for more
              information about our use of data and your rights.
            </p>
            <Button type="submit">
              Save settings
            </Button>
          </form>
        </div>
      </>
    )
  }
  if (displayBanner) {
    return (
      <div className="cookie-banner">
        <div className="cookie-banner__content">
          <p>
            <strong>Our use of cookies</strong>
          </p>
          <p>
            We use cookies to help provide you with the best possible online experience
          </p>
        </div>
        <div className="cookie-banner-buttons">
          <StyledLink internalLink={true} onClick={handleAcceptAll}>
            YES I ACCEPT
          </StyledLink>
          <StyledLink styleType="no-background" internalLink={true} onClick={toggleCookiePannel}>
            NO, GIVE ME MORE INFORMATION
          </StyledLink>
        </div>
      </div>
    )
  }
  return null;
}
