import React, {useState, useEffect} from 'react';
import SwiperCore, {Navigation} from 'swiper';
import {Swiper, SwiperSlide} from 'swiper/react';


SwiperCore.use([Navigation]);
export default function Pager({pages}) {
  const [displayLeftChevron, setDisplayLeftChevron] = useState(false);
  const [displayRightChevron, setDisplayRightChevron]= useState(true);
  const totalPages = 14;
  const maxPages = 9;

  const handleSlideChange = function(swiper){
    setDisplayLeftChevron(!swiper.isBeginning);
    setDisplayRightChevron(!swiper.isEnd);
    SwiperCore.use([Navigation]);
  };

  useEffect(() => {
  }, []);

  const config = {
    spaceBetween: 0,
    slidesPerView: 9,
    preventClicks: false,
    preventClicksPropagation: false,
  };
  return (
    <div className="pager">
        <div className={`pager__carousel-control ${displayLeftChevron ? 'active' :''}`}>
          <img className="button--previous" src="/icons/chevron-yellow-left.svg"/>
        </div>
      <Swiper
        navigation={{
          nextEl: '.button--next',
          prevEl: '.button--previous',
        }}
        {...config}
        onSlideChangeTransitionEnd={handleSlideChange}
      >
        <SwiperSlide>
          <div className="pager__list-item page--active">1</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">2</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">3</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">4</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">5</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">6</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">7</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">8</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">9</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">10</div>
        </SwiperSlide>
        <SwiperSlide>
          <div className="pager__list-item">11</div>
        </SwiperSlide>
      </Swiper>
        <div className={`pager__carousel-control ${displayRightChevron ? 'active' :''}`}>
          <img className="button--next" src="/icons/chevron-yellow-right.svg"/>
        </div>
    </div>
  )
};
