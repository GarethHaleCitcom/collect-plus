import React from 'react';
import PropTypes from 'prop-types';
export default function SearchIcon({className}) {
  return (
    <svg className={`search-icon ${className}`}
         version="1.1"
         xmlns="http://www.w3.org/2000/svg"
         x="0px" y="0px"
         viewBox="0 0 35.9 35.9"
    >
      <path className="search-icon-path" d="M35.9,32.7l-7.5-7.5c2.1-2.7,3.2-6.1,3.2-9.5C31.7,7.1,24.7,0.1,16,0c-0.1,0-0.1,0-0.2,0
	C7.1-0.1,0.1,6.9,0,15.6c0,0.1,0,0.1,0,0.2c-0.1,8.7,6.9,15.8,15.6,15.8c0.1,0,0.1,0,0.2,0c3.4,0,6.8-1.1,9.5-3.2l7.5,7.5L35.9,32.7
	z M4.5,15.8C4.5,9.6,9.4,4.6,15.6,4.5c0.1,0,0.1,0,0.2,0C22,4.5,27,9.4,27.1,15.6c0,0.1,0,0.1,0,0.2C27.2,22,22.2,27,16,27.1
	c-0.1,0-0.1,0-0.2,0C9.6,27.2,4.6,22.2,4.5,16C4.5,16,4.5,15.9,4.5,15.8z"/>
    </svg>
  )
}

SearchIcon.propTypes = {
  className: PropTypes.string,
};

SearchIcon.defaultProps = {
  className: 'icon--default'
};
