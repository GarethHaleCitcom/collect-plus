import React from 'react';
import PropTypes from 'prop-types';

export default function Button({onClick, children, className, type}) {
    return (
        <button type={type} className={`styled-button ${className}`}>
            {children}
        </button>
    )
}

Button.propTypes = {
    onClick: PropTypes.func,
    className: PropTypes.string,
};

Button.defaultProps = {
  onClick: null,
  className: 'button--default',
};
