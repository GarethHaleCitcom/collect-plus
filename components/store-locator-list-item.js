import React, {useState} from "react";

export default function StoreLocatorListItem({onClick, store, index}) {
    const [active, setActive] = useState(false);

    const toggleActive = active => void setActive(active);

    const onListItemMouseEnter = id => {
        // Event.emit('list_item.mouseenter', id);
    };

    const onListItemMouseLeave = () => {
        // Event.emit('list_item.mouseleave');
    };

    const handleClick = () => void onClick && onClick(store);

    return (
        <li className={`list-group-item${active ? ' hovered' : ''}`}
            onMouseEnter={() => onListItemMouseEnter(store.id)}
            onMouseLeave={onListItemMouseLeave}
            onClick={handleClick}
        >
            <div className="counter">{index + 1}</div>
            <div className="storeInfo text-left">
                <strong>{store.site_name}</strong><br/>
                Open until {store[`${store.today}_close_formatted`]}<br/>
                {Number(store.distance).toFixed(2)} miles away
            </div>
            <div className="infoButtons text-right">
                <button type="button" aria-label="Store Information"/>
                <a target="_blank"
                   onClick={event => event.stopPropagation()}
                   href={`https://www.google.com/maps/dir/Current+Location/${store.latitude},${store.longitude}`}
                   className="getDirectionsButton text-center"
                   aria-label="Get directions"
                />
            </div>
        </li>
    )
};
