import React from 'react';
import PropTypes from 'prop-types';
import StyledLink from "./styled-link";
import StyledContent from "./styled-content";
import ScrollAnimation from "./scroll-animation";

export default function BlogPostPreview({title, description, slug, imageSrc, linkText, imageLayoutOrder, contentLayoutOrder}) {
  return (
    <ScrollAnimation className="blog-post-preview">
      <div className="flex flex-wrap">
        <div className={`w-full sm:w-1/2 blog-post-preview--layout ${contentLayoutOrder}`}>
          <div className="blog-post-preview__content">
            <StyledContent description={description} title={title}/>
            <div className="blog-post-preview__link">
              <StyledLink href={`/blogs/${slug}`} styleType="rounded-gray">
                {linkText}
              </StyledLink>
            </div>
          </div>
        </div>
        <div className={`w-full sm:w-1/2 blog-post-preview--layout ${imageLayoutOrder}`}>
          <div className="blog-post-preview__image">
            <img id="fade-in-on-scroll" className="blog-post-preview__image-image" src={imageSrc}/>
          </div>
        </div>
      </div>
    </ScrollAnimation>
  )
}

BlogPostPreview.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  slug: PropTypes.string.isRequired,
  imageSrc: PropTypes.string.isRequired,
  linkText: PropTypes.string,
  imageLayoutOrder: PropTypes.string,
  contentLayoutOrder: PropTypes.string,
};

BlogPostPreview.defaultProps = {
  linkText: 'Read More',
  imageLayoutOrder: 'order-2',
  contentLayoutOrder: 'order-1',
};
