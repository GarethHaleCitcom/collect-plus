import React, {useState, useEffect, useContext} from 'react';
import SearchInput from "./search-input";
import TrackOverlay from "./track-overlay";

const COOKIE_NAME = "parcel-access-token";
export default function Track() {
  const [value, setValue] = useState('');
  const [data, setData] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  const handleChange = (e) => {
    setValue(e.target.value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    (async () => {
      const res = await fetch(`/api/parcel-tracking`, {
        method: "POST",
        body: JSON.stringify({
          trackingCode: value,
        })
      });
      const data = await res.json();
      if(data.Message) {
        setErrorMessage(data.Message);
      }
      if(data.errors) {
        return;
      }
      setData(data[0]);
    })()
  };

  const hideOverlay = () => {
    setData(null);
  };

  return (
    <div className="track">
      {!data &&
      <div className="container-medium">
        <SearchInput placeholder="Enter Tracking Code"
                     title="Track a Parcel"
                     onChange={handleChange}
                     value={value} onSubmit={handleSubmit}
                     errorMessage={errorMessage}
                     text="Enter your Collect+ tracking code in the box above."
        />
      </div>}
      {data && <TrackOverlay onClosePressed={() => hideOverlay()} title={data.BarcodeStatus} clientId={data.ParcelClientID} linkHref={data.TrackingURL}/>}
    </div>
  )
}
