import React from 'react';
import PropTypes from 'prop-types';
import Link from "next/link";
export default function StyledLink({children, href, styleType, internalLink, onClick}) {
  const handleClick = (e) => {
    if(onClick) {
      e.preventDefault();
      onClick(e);
    }
  };
    if(internalLink) {
      return (
        <a href={href} target="_blank" onClick={handleClick} className={`styled-link link--${styleType}`}>
          {children}
        </a>
      )
    }
    return (
        <Link target="_blank" href={href} passRef>
            <a className={`styled-link link--${styleType}`}>
            {children}
            </a>
        </Link>
    )
}

StyledLink.propTypes = {
    href: PropTypes.string,
    styleType: PropTypes.string,
    internalLink: PropTypes.bool,
    onClick: PropTypes.func,
};

StyledLink.defaultProps = {
  styleType: 'default',
  onClick: null,
  href: null,
  internalLink: false
};

