import React, {Fragment, useContext} from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Partners from "../components/partners";
import Trustpilot from "../components/trustpilot";
import ScrollAnimation from "../components/scroll-animation";
import {AppContext} from "../config/context";
import {isDOMAvailable} from "../app/helpers";

export default function HelpAndAdvice({content}) {
  const {helpAndAdvice} = useContext(AppContext);

  return (
    <Screen name="help-and-advice">
      <ScrollAnimation delay={10} className="help-and-advice">
        <Header backgroundImage="/headers/help-and-advice-header.jpg" title="Help & Advice"/>
        <div className="container-medium">
          <div className="flex flex-wrap">
            {
              isDOMAvailable() &&
              helpAndAdvice.map(({title, description, secondDescription, secondTitle, secondList, imageSrc, list}, key) => {
                return (
                  <Fragment key={key}>
                    <div className="w-full lg:w-1/2">
                      <div className="help-and-advice__content">
                        {title && <h1>{title}</h1>}
                        {<p dangerouslySetInnerHTML={{__html: description}}/>}
                        {list && <ul>{list.map((item, key) => <li key={`HELP_AND_ADVICE_LIST_ITEM_KEY_${key}`} dangerouslySetInnerHTML={{__html: item}}/>)}</ul>}
                        {<h1>{secondTitle}</h1>}
                        {<p dangerouslySetInnerHTML={{__html: secondDescription}}/>}
                        {secondList && <ul>{secondList.map((item, key) => <li key={`HELP_AND_ADVICE_LIST_ITEM_KEY_${key}`} dangerouslySetInnerHTML={{__html: item}}/>)}</ul>}
                      </div>
                    </div>
                    <div className="w-full lg:w-1/2 image--layout">
                      <div className="help-and-advice-image" style={{backgroundImage: `url(${imageSrc})`}}/>
                    </div>
                  </Fragment>
                )
              })
            }
          </div>
        </div>
        <div className="container-medium">
          <Trustpilot/>
          <Partners/>
        </div>
      </ScrollAnimation>
    </Screen>
  )
};
