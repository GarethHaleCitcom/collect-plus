import fs from "fs";
import path from "path";
const CLIENT_ID = process.env.PARCEL_API_CLIENT_ID;
const SECRET_KEY = process.env.PARCEL_API_SECRET_KEY;
const API_ENDPOINT = process.env.PARCEL_ENDPOINT;
const AUTH_FILE_PATH = path.resolve(process.cwd(), "storage", "auth.json");
const CACHE_FILE_PATH = path.resolve(process.cwd(), "storage", "cache.json");

async function updateAccessToken() {
  const res = await fetch(`${API_ENDPOINT}/api/GetClientAccesstoken`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
    },
    body: JSON.stringify([
      {
        ClientId: CLIENT_ID,
        SecretKey: SECRET_KEY,
      },
    ]),
  });
  if(res.status !== 200) {
    return null;
  }
  const tokens = await res.json();

  const auth = {
    token: tokens[0]?.accessToken,
    expiry: Math.floor(Date.now() / 1000) + 60000, // 1 hour
  };
  fs.writeFileSync(AUTH_FILE_PATH, JSON.stringify(auth, null, 2), "utf8");
  return auth;
}
async function getAccessToken() {
  if(process.env?.NODE_ENV === 'development') {
    const auth = await updateAccessToken();
    if(!auth) {return null}
    return auth.token;
  }
  try {
    let auth;
    if (!fs.existsSync(AUTH_FILE_PATH)) {
      auth = await updateAccessToken();
      if(!auth) {return null}
    } else {
      auth = JSON.parse(fs.readFileSync(AUTH_FILE_PATH, "utf8"));
      const now = Math.floor(Date.now() / 1000);
      if (auth.expiry <= (new Date()).getTime()) {
        auth = await updateAccessToken();
        if(!auth) {return null}
      }
    }
    return auth.token;
  } catch (e) {
    return null;
  }
}

async function getTrackingInfoFromCache(token, trackingCode) {
  const expiry = (new Date()).getTime() + 15 * (60 * 1000);
  const now = (new Date()).getTime();
  let info;
  let cached = [];
  if(fs.existsSync(CACHE_FILE_PATH)) {
    cached = fs.readFileSync(CACHE_FILE_PATH);

    cached = JSON.parse(cached);
    cached.forEach((item) => {
      if(item.expiry > now && item.trackingCode === trackingCode) {
        info = item.data;
      }
    });
    if(info) {
      return info;
    }
  }

  info = await getTrackingInfo(token, trackingCode);

  fs.writeFileSync(CACHE_FILE_PATH, JSON.stringify([...cached, {id: cached.length + 1, data: info, trackingCode, expiry: expiry}]), 'utf8');

  return info;

}

async function getTrackingInfo(token, trackingCode) {
  try {
    const res = await fetch(`${API_ENDPOINT}/api/GetParcelClientTracking`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify([
        {
          TrackingBarcode: trackingCode,
        },
      ]),
    });
    return await res.json();
  } catch (e) {
    return null;
  }
}
export default async (req, res) => {
  console.log("API_ENDPOINT -> ", API_ENDPOINT);
  const body = JSON.parse(req.body);
  const trackingCode = (body?.trackingCode ?? "").trim();
  if (trackingCode.length === 0) {
    return res.status(422).json({ errors: { server: "No tracking code" } });
  }
  const token = await getAccessToken();
  if(!token){
    return res.status(500).json({ Message: "API error" });
  }
  const info = await getTrackingInfoFromCache(token, trackingCode);
  res.json(info);
};
