import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import StyledContent from "../components/styled-content";

export default function Terms({content}) {
  return (
    <Screen name="terms-and-conditions">
      <div className="terms-and-conditions">
        <Header backgroundImage="/headers/contact-header.jpg" title="Terms and<br/>Conditions"/>
        <div className="container-medium">
          <h1 className="terms-and-conditions-title">
            Collectplus.co.uk Website terms<br/>
          </h1>
          <ol>
            <li>This page (together with the documents referred to on it) tells you the terms of use on which you may
              make use of the Collect+ website www.collectplus.co.uk. Please read these terms of use carefully before
              you start to use the website. By using the website, you indicate that you accept these terms of use and
              that you agree to abide by them. If you do not agree to these terms of use, please refrain from using the
              Collect+ website.
            </li>
            <li>The www.collectplus.co.uk is owned and operated by PayPoint Network Limited ("PayPoint"), trading as
              “Collect+”. PayPoint Network Limited is registered in England under company number 02973115 and has its
              registered office at 1 The Boulevard, Shire Park, Welwyn Garden City, Hertfordshire AL7 1EL.
            </li>
            <li>We make no promise that this site is appropriate or available for use in locations outside of the UK. It
              is the responsibility of any person accessing this website in any jurisdiction to satisfy themselves as to
              the full observance of the laws and regulatory requirements of the relevant jurisdiction, including the
              obtaining of governmental or other consents which may be required or observing any other formality or
              restriction that needs to be observed in such jurisdiction. Any failure to comply with any formality or
              failure to observe any restriction may constitute a violation of the securities laws of any such
              jurisdiction.
            </li>
            <li>"Collect+" is a trademark of Collect+ Brand Limited, a wholly owned subsidiary of ultimate parent
              company PayPoint Plc, the parent company of PayPoint Network Limited. Otherwise, the intellectual property
              rights in this and in any text, images, video, audio or other multimedia content, software or other
              information or material submitted to or accessible from this site (save for any third party websites) are
              owned by PayPoint Network Limited and its licensors.
            </li>
            <li>PayPoint Network Limited, Collect+ Brand Limited and their licensors reserve all intellectual property
              rights (including, but not limited to, all copyright, trade marks, domain names, design rights, database
              rights, patents and all other intellectual property rights of any kind) whether registered or unregistered
              anywhere in the world.
            </li>
            <li>Nothing in these terms grants you any legal rights in this site or its content other than as necessary
              for you to access it. You agree not to adjust, try to circumvent or delete any notices contained on the
              this site or its content (including any intellectual property notices) and in particular, in any digital
              rights or other security technology embedded or contained within this site or its content.
            </li>
            <li>This site may viewed for the purpose of personal reference, but may not be permanently reproduced,
              stored or copied for further sale or distribution. This site may not be used for any commercial purposes
              without first obtaining prior written consent from PayPoint. This includes any form of ‘scraping’ or
              automated or manual collection of information contained on this site, including (without limitation) its
              store locator or tracking functions.
            </li>
            <li>Electronic links to this website are prohibited without the consent of PayPoint. You may not frame this
              website without the consent of PayPoint. Certain links on this website may lead to third party websites
              and/or assets located on servers maintained by third parties over whom PayPoint has no control. PayPoint
              makes no representation as to the accuracy or any other aspect of, and accepts no liability of any kind
              for, use of such websites, the information contained on such websites or in such assets or on such
              servers.
            </li>
            <li>For information on how PayPoint uses personal data and cookies on www.collectplus.co.uk, please see:<br/>
              <a target="_blank"
                 href="https://www.google.com/maps/place/The+Blvd,+Welwyn+Garden+City+AL7+1EL,+UK/@51.8080528,-0.1933535,18.03z/data=!4m5!3m4!1s0x48763b2ac248b11f:0x1cc55be33f14c385!8m2!3d51.8082158!4d-0.1918955">
                PayPoint plc 1<br/>
                The Boulevard Shire Park,<br/>
                Welwyn Garden City, <br/>
                Herts, AL7 1EL
              </a>
            </li>
            <li>PayPoint seeks to ensure that the information on this website (other than information accessed by
              hypertext links) is accurate. However, no representation or warranty is made and no liability is accepted
              as to the accuracy, completeness or any other aspect of the information or links contained on the website.
              Any and all liability which might arise from your use or reliance on the information or links contained on
              the website is excluded unless such liability results from fraudulent misrepresentation or is for death or
              personal injury caused by negligence.
            </li>
            <li>You are reminded that the internet is an inherently unstable medium and the risks of interruption,
              interference and disruption mean that the information or links contained on this website may in fact be
              outside the control of PayPoint. In addition, errors, omissions, interruptions and delays of service may
              occur at any time, and PayPoint accepts no obligation or responsibility in respect of such events.
              PayPoint may suspend or terminate access or operation of this site at any time as it sees fit.
            </li>
            <li>The material contained in this website is a guide for general information purposes only. To the extent
              permitted by law, any and all liability which might arise from your use or reliance on the information or
              links contained on the website is excluded.
            </li>
            <li>PayPoint may revise these terms of use at any time by amending this page. You are expected to check this
              page from time to time to take notice of any changes PayPoint makes, as they are binding on you. Some of
              the provisions contained in these terms of use may also be superseded by provisions or notices published
              elsewhere on the website.
            </li>
            <li>For the terms of business applicable to any shipping and/or collection of a parcel using the Collect+
              store network, please refer to the retailer or carrier with whom you initially made your purchase
              involving a Collect+ store.
            </li>
            <li>Services featured on this website may not be available in all Collect+ stores. Please check with the
              retailer or carrier you purchase your goods or delivery services with as to your local participating
              stores.
            </li>
          </ol>
          <h2 className="terms-and-conditions-subtitle">Legal Information</h2>
          <p>
            PayPoint Network Limited is a public limited company (Company Registration Number 02973115) whose registered office is at 1 The Boulevard, Shire Park, Welwyn Garden City, Hertfordshire, England, AL7 1EL.
          </p>
          <p>
            For additional contact information please <b><a href="mailto:info@collectplus.co.uk">info@collectplus.co.uk</a> </b>
          </p>
        </div>
      </div>
    </Screen>
  );
};

Terms.defaultProps = {
  content: {
    firstColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    },
    secondColumn: {
      title: 'Title Here',
      description: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.</p>',
    }
  }
};
