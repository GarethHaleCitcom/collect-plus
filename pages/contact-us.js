import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Partners from "../components/partners";
import StyledContent from "../components/styled-content";
import ScrollAnimation from "../components/scroll-animation";

export default function ContactUs({content}) {
  return (
    <Screen name="contact">
      <ScrollAnimation className="contact">
        <Header backgroundImage="/headers/contact-header.jpg" title="Contact"/>
        <div className="container-medium">
          <div className="flex flex-wrap first-half">
            <div className="w-full sm:w-1/2">
              <div className="contact-first-content">
                <StyledContent
                  title="Have a question/query about our instore experience?"
                  descriptionAsHtml={true}
                  description={`Email: <a href="mailto:info@collectplus.co.uk">info@collectplus.co.uk</a>`}
                />
                <StyledContent
                  title="Press Office Enquiry"
                  descriptionAsHtml={true}
                  description={`Email: <a href="mailto:collectplus@mhpc.com">collectplus@mhpc.com</a>`}
                />
              </div>
            </div>
            <div className="w-full sm:w-1/2">
              <div id="fade-in-on-scroll" className="contact-first-image" style={{backgroundImage: `url(/contact-us/contact-us.jpg)`}}/>
            </div>
          </div>
        </div>
        <div className="container-medium">
          <Partners title="Question about your parcel delivery/condition of your parcel?" styleType="title--orange contact--size" description="Contact the relevant partner:"/>
        </div>
      </ScrollAnimation>
    </Screen>
  )
};

ContactUs.defaultProps = {
  content: {
    firstColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    },
    secondColumn: {
      title: 'Title Here',
      description: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.</p>',
    }
  }
};
