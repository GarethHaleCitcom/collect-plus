import React from 'react';
import Screen from '../components/screen';
import sitemap from "../config/sitemap";
import Link from "next/link";
import Header from "../components/header";
export default function Sitemap() {
  return (
    <Screen name="sitemap">
      <Header
        title="Sitemap"
        backgroundImage="/headers/sitemap-header.jpg"
      />
      <div className="sitemap">
        <div className="container-medium">
          <div className="flex flex-wrap">
            <div className="w-full sm:w-1/2">
              <ul className="sitemap-list">
                {
                  sitemap.map((({to, name}, index) => <li key={index}><Link href={to}>{name}</Link></li>))
                }
              </ul>
            </div>
            <div className="w-full sm:w-1/2">
              <div className="sitemap-img"/>
            </div>
          </div>
        </div>
      </div>
    </Screen>
  )
}
