import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import StyledContent from "../components/styled-content";

export default function CookiesPolicy({content}) {
  return (
    <Screen name="cookies-policy">
      <div className="cookies-policy">
        <Header backgroundImage="/headers/contact-header.jpg" title="Cookies Policy"/>
        <div className="container-medium">
          <h2>Cookies policy</h2>
          <p><strong>To make this site work properly, we sometimes place small data files called cookies on your
            device.</strong></p>
          <h2>What are cookies?</h2>
          <p>
            A cookie is a small text file that a website saves on your computer or mobile device when you visit the
            site. It helps the website to remember your actions and preferences (such as login, language, font size and
            other display preferences) over a period of time, so you don't have to keep re-entering them whenever you
            come back to the site or browse from one page to another.
          </p>

          <h2>How do we use cookies?</h2>
          <p>A number of our pages may use cookies to monitor and remember:</p>
          <ul>
            <li>your display preferences, such as contrast colour settings or font size</li>
            <li>if you have agreed (or not) to our use of cookies on this site</li>
            <li>website performance</li>
            <li>user traffic to pages</li>
          </ul>
          <p>There is a list of the cookies we use set out below. The cookies are classed as:</p>
          <ul>
            <li>Strictly necessary cookies</li>
            <li>Functional cookies</li>
            <li>Performance cookies</li>
            <li>Advertising Cookies – we do not use advertising cookies on our website.</li>
          </ul>
          <h2>Do we use other cookies?</h2>
          <p>
            Some of our pages or sub-sites may use additional or different cookies to the ones described above. If so,
            the details of these will be provided in their specific cookies notice page. So you may be asked for your
            agreement to store these cookies.
          </p>
          <h2>Just browsing?</h2>
          <p>If you want to know more about how we use your data please see our Privacy Statement.</p>
          <h2>How to control cookies</h2>
          <p>
            You can control and/or delete cookies as you wish - for details, see <a
            href="AllAboutCookies.org">AllAboutCookies.org</a>. You can
            delete all cookies that are already on your computer and you can set most browsers to prevent them from
            being placed.
          </p>
          <div className="table-wrapper">
            <table>
              <thead>
              <tr>
                <th>Location</th>
                <th>Classification</th>
                <th>Name</th>
                <th>What is it used for?</th>
              </tr>
              </thead>
              <tbody>
              <tr>
                <td>
                  PayPoint
                </td>
                <td>
                  Performance
                </td>
                <td>
                  _ga
                </td>
                <td>
                  Allows PayPoint to distinguish individual users and collect usage metrics from the user. Persists for
                  2
                  calendar years unless deleted.
                </td>
              </tr>
              <tr>
                <td>PayPoint</td>
                <td>Performance</td>
                <td>_gid</td>
                <td>We use this to collect usage metrics from users, which helps inform future product enhancements.
                  Expires
                  within 24 hours unless deleted.
                </td>
              </tr>
              <tr>
                <td>PayPoint</td>
                <td>Performance</td>
                <td>_gat_gtag_</td>
                <td>We use this to control or throttle the request rate by a user in a specific period of time. Expires
                  after 1
                  minute.
                </td>
              </tr>
              <tr>
                <td>PayPoint</td>
                <td>Strictly Necessary</td>
                <td>cookie_settings</td>
                <td>We will use this cookie to record the consent of the user with the use of other associated cookies,
                  such
                  that a cookies notice can be hidden on future visits to the website, and a user’s preference to
                  disable
                  non-essential cookies is recorded for future use. Expires after 6 months unless deleted.
                </td>
              </tr>
              <tr>
                <td>
                  PayPoint
                </td>
                <td>
                  Strictly Necessary
                </td>
                <td>
                  cookie_settings
                </td>
                <td>
                  We will use this cookie to storethe user’s response to theservice message, such that themessage is
                  hidden on futurevisits to the website by the user.Expires after 6 months unlessdeleted.
                </td>
              </tr>
              </tbody>
            </table>
          </div>
          <p>
            If you choose to manage or delete the cookies used, you may have to manually adjust some preferences every
            time you visit a site and some services and functionalities may not work.
          </p>
          <h2>Cookies in more detail</h2>
          <p>The rest of this section gives you a list of what performance cookies are set by our website and a
            description of how they are used.</p>
          <h2>Analytics software</h2>
          <p>
            PayPoint use hosted web analytics software. These are hosted services provided and managed by third party
            companies.
          </p>
          <p>
            ‘Performance' cookies collect information about how you use our website - e.g. which pages you
            visit and if you experience any errors. These cookies do not collect any information that could identify you
            and are used to help improve how our website works and measure how effective the content is.
          </p>
          <p>
            PayPoint’s main
            supplier of hosted web analytics and recordings is Google, Inc.. This website uses Google Analytics, a web
            analytics service provided by Google, Inc. ("Google").
            Google Analytics uses "cookies", which are text files placed on your computer, to help the website analyse
            how users use the site. This is to help inform future product enhancements and improvements, and to diagnose
            any defects quicker.
          </p>
          <p>
            The information generated by the cookie regarding our websites (including IP addresses)
            will be transmitted to and stored by Google on servers that may be hosted in the United States. Google will
            use this information for the purpose of evaluating use of our website, compiling reports on website activity
            for website operators and providing other related services to PayPoint. Please visit <a target="_blank"
                                                                                                    href="www.google.com">Google
            website</a> for more
            information regarding their privacy policy.
          </p>
          <p>
            By accepting these cookies you consent to the processing of data about you by Google, Inc. and in the manner
            and for the purposes set out above.
          </p>
        </div>
      </div>
    </Screen>
  )
};
