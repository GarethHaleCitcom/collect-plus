import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Partners from "../components/partners";
import dynamic from 'next/dynamic'

const StoreLocatorComponent = dynamic(() => import('../components/store-locator'), {ssr: false});
const Trustpilot = dynamic(() => import('../components/trustpilot'), {ssr: false});

export default function FindAStore({content}) {
  return (
    <Screen name="find-a-store">
      <div className="find-a-store">
        <Header backgroundImage="/headers/find-store-header.jpg" title="Find a Store"/>
        <section className="home__find-a-store">
          <StoreLocatorComponent/>
        </section>
        <div className="container-medium">
          <Trustpilot/>
          <Partners/>
        </div>
      </div>
    </Screen>
  )
};

FindAStore.defaultProps = {
  content: {
    firstColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    },
    secondColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    }
  }
};
