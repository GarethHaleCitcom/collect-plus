import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import StyledContent from "../components/styled-content";

export default function Privacy() {
  return (
    <Screen name="privacy-policy">
      <div className="privacy-policy">
        <Header backgroundImage="/headers/contact-header.jpg" title="Privacy Policy"/>
        <div className="container-medium privacy-policy-body">
          <h1>PayPoint Collect+ Privacy Statement</h1>
          <h2>When is information collected?</h2>
          <p>The website www.collectplus.co.uk is operated by PayPoint Network Limited, under company number 02973115 with our
            registered office at 1 The Boulevard, Shire Park, Welwyn Garden City, Hertfordshire AL7 1EL.</p>
          <p>This Privacy Statement tells you what to expect when the PayPoint Group (“PayPoint”) collects personal information
            about users of the Collect+ website, and applies to the information we collect from that website.
            It is intended to give you a better understanding of the data we use and what we do with your data.</p>

          <h2>Definitions</h2>

          <p>
            For the purpose of this Privacy Notice;<br/>
            Client means an organisation using a PayPoint service to facilitate the send and receipt of parcels on behalf of
            their customer (including but is not limited to, the Client carrier).
            Customer means a consumer or business that is a customer of a client.
            PayPoint means a member of the PayPoint Group.
          </p>

          <h2>On what lawful basis will we use your information?</h2>

          <p>We will only use your information where we have your consent or we have another lawful reason for using it. These
            reasons include where we:</p>
          <ul>
            <li>need to pursue our legitimate interests;</li>
            <li>need to process the information to carry out an agreement we have with you;</li>
            <li>need to process the information to provide a service on behalf of the Client carrier;</li>
            <li>need to process the information to comply with a legal obligation;</li>
            <li>believe the use of your information as described is in the public interest, e.g. for the purpose of preventing
              or detecting crime;
            </li>
            <li>need to establish, exercise or defend our legal rights; or</li>
            <li>need to use your information for insurance purposes.</li>
          </ul>

          <h2>Disclosure of personal information</h2>

          <p>We may from time to time disclose information as follows;</p>
          <ul>
            <li>Your personal data to appropriate investigatory and regulatory authorities.</li>
            <li>Your information to a supplier if required to use a PayPoint service or obtain support for PayPoint services.
            </li>
            <li>If you are a customer of a client we may provide your information to your service provider (including, but not
              limited to, the Client carrier) and the appropriate authorities.
            </li>
          </ul>

          <h2>Monitoring</h2>

          <p>PayPoint electronic communication systems are monitored without user consent to: investigate or detect unauthorised
            use; prevent or detect crime; establish the existence of facts relevant to PayPoint; or ascertain compliance with
            regulatory practices relevant to the business of PayPoint.</p>

          <p>Calls to PayPoint are recorded for operational and training purposes. Calls are not processed against the individual
            data subject.</p>


          <h1>Website Users</h1>

          <h2>Customer Information</h2>
          <p>When you use our website, we may collect:</p>
          <li>information that you provide by filling in forms on the website, for example your contact details or parcel tracking
            number;
          </li>
          <li>
            details of your visits to the website, the resources you access (please see Cookies Policy for more
            information);<br/>
            Where you provide us with personal data we are the data controller of any such data. This means we are responsible
            for, and control the processing of, your personal information.
          </li>


          <h2>Parcel Tracking</h2>
          <p>Where information is provided by you in order to undertake parcel tracking services, such as a unique tracking
            number, we may provide you with a link to our Client carrier’s website for you to continue to track your parcel
            directly. In such instance, any personal data you have provided will be transferred to that page.</p>

          <h2>Data held by Clients</h2>
          <p>If you are a Customer of a Client and have any questions in relation to any personal data you have provided to a
            Client directly, such as the privacy policy applicable to such data, please contact the Client directly. PayPoint is
            unable to disclose any such personal data unless instructed to do so by the Client as we cannot verify the data
            subject.</p>

          <h2>Data export or transfer</h2>

          <p>In order to provide services or support to Clients and Customers, PayPoint may use service providers implementing
            “cloud” based and other technology. These services may include operational, technical and administrative support
            that is based outside the EEA. Any transfer of personal data that is outside the EEA will be in accordance with the
            recommended data transfer models applicable at the time.</p>


          <h2>Complaints or queries</h2>

          <p>PayPoint applies transparency when collecting and using personal information. If you have concerns over the use of
            your personal data please contact us.</p>

          <p>This Privacy Statement are not an exhaustive list of all aspects of PayPoint’s collection and use of personal
            information. If you wish to have any additional information or explanation please contact us via the contact details
            set out below.</p>

          <p>If you want to request information about our privacy policies or a complaint you can e-mail us at
            privacy@paypoint.com  or write to us at:</p>
          <p>
            Data Protection Officer<br/>
            PayPoint Group<br/>
            1 The Boulevard<br/>
            Shire Park<br/>
            Welwyn Garden City<br/>
            Hertfordshire<br/>
            AL7 1EL
          </p>

          <p>If you have a complaint in relation to the use of your personal data you may contact us in the first instance. If you
            are still not happy we have resolved the matter you may also contact the Information Commissioner’s Office. Contact
            details are available on the Information Commissioner’s website.</p>

          <h2>Data Subject Access Requests</h2>

          <p>Individuals can find out if we hold any personal information by making a “subject access request” under the Data
            Protection Act 2018. If we do hold information about you we will:</p>
          <ul>
            <li>give you a description of it;</li>
            <li>tell you why we are holding it;</li>
            <li>tell you who it could be disclosed to; and</li>
            <li>let you have a copy of the information in an intelligible form.</li>
          </ul>
          <p>
            To make a request to PayPoint for any personal information we may hold, you need to put the request in writing
            addressing it to our Data Protection Officer at the address provided below. We cannot disclose information in
            relation to other data subjects, client information or client customer information under such a request. We will
            remove information relating to other data subject or other organisation from the information we send you.
          </p>

          <p>
            If you agree, we will try to deal with your request informally, for example by providing you with the specific
            information you need over the telephone. If you have e-mailed us we will respond by e-mail. Please note e-mail
            communications are no guaranteed to be secure due to the nature of the communication method.
          </p>

          <p>
            The Information Commissioner has provided a guide on its website to data subject access requests. This provides
            indications of the circumstances when an access request can be limited or declined. For example, a data subject
            access request does not entitle the data subject to information relating to or confidential to a third party. Data
            subject access requests are available to individuals but not businesses.
          </p>

          <h2>Other use of your rights.</h2>
          <p>
            Under the Data Protection Act 2018, you have rights as an individual which you can exercise in relation to the
            information we hold about you. You can read more about these rights here
            – https://ico.org.uk/for-the-public/is-my-information-being-handled-correctly/
          </p>

          <h1>General</h1>

          <h2>People who e-mail us</h2>
          <p>
            Any e-mail sent to us, including any attachments, may be monitored and used by us for reasons of security and for
            monitoring compliance with PayPoint policies.  E-mail monitoring or blocking software may also be used. Please be
            aware that you have a responsibility to ensure that any e-mail you send to us is within the bounds of the law.
          </p>

          <h2>
            Visitors to our other websites
          </h2>
          <p>
            When you visit our website www.paypoint.com, or any other PayPoint website, please see the specific website privacy
            notice for details of cookies and other information specific to that website.
          </p>

          <h2>Links to other websites</h2>
          <p>
            This Privacy Notice does not cover the links within any PayPoint website or documentation linking to other websites.
            We encourage you to read the privacy statements on any other websites you visit.
          </p>

          <h2>Keeping your information secure</h2>
          <p>
            PayPoint takes its obligations to keep data secure seriously. It is ISO 27001 accredited and a PCI compliant
            organisation for the purpose of processing card payment data.
          </p>

          <h2>Governing Law</h2>
          <p>
            This Privacy Notice shall be governed by and construed in accordance with English law and the parties hereby submit
            to the exclusive jurisdiction of the English courts in respect of all matters arising in connection herewith.
          </p>

          <h2>Retention</h2>
          <p>
            Information we hold in relation to payments is retained for statutory and regulatory purposes (including tax
            records). Payment information may be retained for periods of 3 or 5 years or such other period as required by law.
            Information relating to tax records may be held for 7 years or such other period are required by law. Where we hold
            information on behalf of a Client, the retention period is as specified by the Client. We can provide more
            information on retention periods for specific types of information if you email privacy@paypoint.com . Our retention
            policy is updated from time to time to reflect changes in our obligations and legal duties.
          </p>

          <h2>How to contact us</h2>

          <p>If you want to request information about our privacy policies or a complaint you can e-mail us at
            <a href="mailto:privacy@paypoint.com">privacy@paypoint.com</a>  or write to us at:</p>
          <a target="_blank" href="https://www.google.com/maps/place/The+Blvd,+Welwyn+Garden+City+AL7+1EL,+UK/@51.8080474,-0.1932547,18z/data=!3m1!4b1!4m5!3m4!1s0x48763b2ac248b11f:0x1cc55be33f14c385!8m2!3d51.8082158!4d-0.1918955">
            Data Protection Officer<br/>
            PayPoint Group<br/>
            1 The Boulevard<br/>
            Shire Park<br/>
            Welwyn Garden City<br/>
            Hertfordshire<br/>
            AL7 1EL
          </a>
          <p>
            If you are not happy with our response to any query or wish to complain you may also contact the Information
            Commissioner’s Office via <a target="_blank" href="https://ico.org.uk/concerns/">https://ico.org.uk/concerns/</a> or
            by using the contact details at
            <a target="_blank" href="https://ico.org.uk/global/contact-us/">https://ico.org.uk/global/contact-us/</a>
          </p>

          <h2>Changes and Updates</h2>
          <p>We may update this Privacy Statement from time to time and in particular as the legal environment in relation to your
            information changes.</p>

          <p>This Privacy Statement was last updated on 29 September 2020</p>

        </div>
      </div>
    </Screen>
  )
};
