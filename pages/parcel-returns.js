import React from 'react';
import Screen from '../components/screen';
import PropTypes from 'prop-types';
import ParcelReturn from "../components/parcel-return";
import Partners from "../components/partners";
import Header from "../components/header";
import dynamic from "next/dynamic";
import Link from "next/link";

const Trustpilot = dynamic(() => import('../components/trustpilot'), {ssr: false});
const StoreLocator = dynamic(() => import('../components/store-locator'), {ssr: false});
export default function Returns({meta}) {
  return (
    <Screen meta={meta}>
      <Header backgroundImage="/headers/returns-header.jpg" title="Returns"/>
      <div className="container-medium">
        <Partners asLinks={true} title="Easy returns with one of our partners" description="Drop off your returns to one of our <b>thousands</b> of locations open early until late, 7 days a week."
                  styleType="title--orange"/>
      </div>
      <ParcelReturn hasBorder={true}/>
      <div className="container-medium">
        <div className="returns__benefits">
          <div className="flex flex-wrap">
            <div className="w-full sm:w-1/2 benefits--layout">
              <h2 className="returns__benefits-title">
                Why use Collect+ for returns?
              </h2>
              <ul className="returns__benefits__list">
                <li className="returns__benefits__list-item">We have thousands of stores nationwide where you can drop off your returns. Most are open early 'til late 7 days a week</li>
                <li className="returns__benefits__list-item">Most returns are free, check with your retailer your purchased the item from for details</li>
                <li className="returns__benefits__list-item">Track your parcel back to the retailer with a unique code</li>
                <li className="returns__benefits__list-item">Collect+ is highly rated from customer reviews on Trustpilot</li>
              </ul>
            </div>
            <div className="w-full sm:w-1/2 benefits--layout">
              <h2 className="returns__benefits-title">
                Thousands of Collect+ stores nationwide
              </h2>
              <p className="returns__benefits-description">
                Drop it off at your local Collect+ store. Check the <Link href="/#find-a-store"><span>Find a Store tab</span></Link> to check which stores accept which partners.
              </p>
            </div>
          </div>
        </div>
      </div>
      <StoreLocator/>
      <div className="container-medium">
        <Trustpilot/>
      </div>
    </Screen>
  );
}

Returns.propTypes = {
  meta: PropTypes.object
};

Returns.defaultProps = {
  meta: {
    title: "Collect plus - Returns",
    description: "Lorem ipsum"
  }
};
