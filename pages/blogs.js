import React, {useContext} from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Partners from "../components/partners";
import Trustpilot from "../components/trustpilot";
import {AppContext} from "../config/context";
import BlogPostPreview from "../components/blog-post-preview";
import Pager from "../components/Pager";
import BlogCategoriesFilter from "../components/blog-categories-filter";

export default function Blogs({content}) {
  const {blogs} = useContext(AppContext);
  return (
    <Screen name="blog">
      <div className="blog">
        <Header backgroundImage="/headers/blog-header.jpg" title="Blog"/>
        <div className="container-medium">
          {/*<BlogCategoriesFilter/>*/}
          {
            blogs.map(({data: {preview: {description, title, imageSrc}, slug}}, key) => {
              return (
                <BlogPostPreview key={key}
                                 description={description}
                                 slug={slug}
                                 title={title}
                                 imageLayoutOrder={key % 2 === 1 ? 'order-2 sm:order-1' : 'order-2'}
                                 contentLayoutOrder={key % 2 === 1 ? 'order-1 sm:order-2' : 'order-1'}
                                 imageSrc={imageSrc}/>
              )
            })
          }
        </div>
        {/*<Pager/>*/}
        <div className="container-medium">
          <Trustpilot/>
          <Partners/>
        </div>
      </div>
    </Screen>
  )
};
