import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import StyledContent from "../components/styled-content";

export default function ServiceMessage({content}) {
  return (
    <Screen name="privacy-policy">
      <div className="privacy-policy">
        <Header backgroundImage="/headers/header.jpg" title="Service Message </br> Covid-19"/>
        <div className="container-medium">
          <div className="privacy-policy-content">
            <StyledContent
              title={content.firstColumn.title}
              description={content.firstColumn.description}/>
          </div>
          <div className="privacy-policy-content">
            <StyledContent
              title={content.secondColumn.title}
              descriptionAsHtml={true}
              description={content.secondColumn.description}/>
          </div>
          <div className="privacy-policy-content">
            <StyledContent
              title={content.firstColumn.title}
              description={content.firstColumn.description}/>
          </div>
        </div>
      </div>
    </Screen>
  )
};

ServiceMessage.defaultProps = {
  content: {
    firstColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    },
    secondColumn: {
      title: 'Title Here',
      description: '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.</p><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.</p>',
    }
  }
};
