import React, {useContext, useState, useEffect} from 'react';
import moment from "moment";
import Screen from '../../components/screen';
import Trustpilot from "../../components/trustpilot";
import {useRouter} from "next/router";
import Partners from "../../components/partners";
import {AppContext} from "../../config/context";
import StyledLink from "../../components/styled-link";
import Header from "../../components/header";

export default function BlogPost(props) {
  const router = useRouter();
  const [blog, setBlog] = useState(null);
  const {findBlogPost} = useContext(AppContext);
  useEffect(() => {
    setBlog(findBlogPost(router.query.slug));
  }, [router.query.slug]);
  if(!blog) {
    return null;
  }
  return (
    <Screen name="blog-post" meta={blog.meta}>
      <div className="blog-post">
        <Header backgroundImage="/headers/blog-header.jpg" title="Blog"/>
        <div className="container-medium">
          <div className="blog-post-navigation">
            <StyledLink href="/blogs" styleType="rounded-gray">
              Back to blog
            </StyledLink>
          </div>
          {
            blog && (
              <>
                <div className="post-body-container">
                  <div className="blog-post__header">
                    <span className="blog-post__header-date">{blog.data.createdAt}</span>
                    <h1 className="blog-post__header-title">
                      {blog.data.post.title}
                    </h1>
                  </div>
                  <div className="blog-post__body" dangerouslySetInnerHTML={{__html: blog.data.post.body}}/>
                </div>
              </>
            )
          }
        </div>
        <div className="container-medium">
          <Trustpilot/>
          <Partners/>
        </div>
      </div>
    </Screen>
  )
}
