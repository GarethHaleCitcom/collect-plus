import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Partners from "../components/partners";
import StyledContent from "../components/styled-content";
import ScrollAnimation from "../components/scroll-animation";
import dynamic from "next/dynamic";

const Trustpilot = dynamic(() => import("../components/trustpilot"), {ssr: false});
export default function About({content}) {
  return (
    <Screen name="about">
      <ScrollAnimation delay={10} className="about">
        <Header backgroundImage="/headers/about-us-header.jpg" title="About us"/>
        <div className="container-medium">
          <div className="flex flex-wrap first-half">
            <div className="w-full sm:w-1/2">
              <div className="about-first-content">
                <StyledContent
                  title="About Collect+"
                  description="Collect+ is a network of locations made up of thousands of newsagents, convenience stores, supermarkets and petrol stations, so wherever you are, there’s a good chance that a Collect+ store is nearby. The majority of Collect+ stores are open 7-days a week, early ’til late, so collecting and sending parcels is easy and convenient."/>
              </div>
              <div className="about-first-content">
                <StyledContent
                  title="With thousands of conveniently located Collect+ stores across the UK, it has never been so easy to drop off or pick up your parcels."/>
                  <StyledContent
                    title="Did you know?"
                    description="94% of urban population live within one mile of a store. 90% of rural population live within five miles of a store."
                    descriptionAsHtml={true}
                  />
              </div>
            </div>
            <div className="w-full sm:w-1/2">
              <div className="about-first-image">
                <img id="fade-in-on-scroll" src="/about/coverage-map.png"/>
              </div>
            </div>
          </div>
          <section className="about__why-collect-plus">
            <h1 className="about__why-collect-plus-title">
              Why Collect+?
            </h1>
            <div className="flex flex-wrap">
              <div className="w-full sm:w-1/3">
                <div className="about__why-collect-plus__card">
                  <img className="about__why-collect-plus__card-image" src="/about/simple-icon-green.png"/>
                  <h4 className="about__why-collect-plus__card-subtitle">
                    Convenience at your fingertips
                  </h4>
                  <p className="about__why-collect-plus__card-description">
                    Collect+ have the Click & Collect and Return services available to you, making online shopping an even easier experience.
                  </p>
                </div>
              </div>
              <div className="w-full sm:w-1/3">
                <div className="about__why-collect-plus__card">
                  <img className="about__why-collect-plus__card-image" src="/about/local-icon.png"/>
                  <h4 className="about__why-collect-plus__card-subtitle">
                    Local
                  </h4>
                  <p className="about__why-collect-plus__card-description">
                    With <b>thousands</b> of stores located nationwide, there is bound to be a Collect+ store nearby.
                  </p>
                </div>
              </div>
              <div className="w-full sm:w-1/3">
                <div className="about__why-collect-plus__card">
                  <img className="about__why-collect-plus__card-image" src="/about/accessible-icon-green.png"/>
                  <h4 className="about__why-collect-plus__card-subtitle">
                    Accessible
                  </h4>
                  <p className="about__why-collect-plus__card-description">
                    When sending, or returning a parcel, you can drop it off at a time that suits you – and track it every step of the way.
                  </p>
                </div>
              </div>
            </div>
          </section>
          <section className="about-notes">
            <p>Our partners provide send services. Please visit their website for more information.</p>
            <p>Not all Collect+ stores offer services from all partners, so please <a href="/find-a-store">click here</a> for more information.</p>
          </section>
        </div>
        <div className="container-medium">
          <Trustpilot/>
          <Partners/>
        </div>
      </ScrollAnimation>
    </Screen>
  )
};

// About.defaultProps = {
//   meta: {
//     title: 'Collect+ | About us'
//   }
// };
