import React from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Partners from "../components/partners";
import Trustpilot from "../components/trustpilot";
import dynamic from 'next/dynamic'
import Track from "../components/track";

export default function TrackAParcel({content}) {
  return (
    <Screen name="track-a-parcel">
      <div className="track-a-parcel">
        <Header backgroundImage="/headers/track-a-parcel-header.jpg" title="Track a Parcel"/>
        <section className="home__find-a-store">
          <Track/>
        </section>
        <div className="container-medium">
          <Trustpilot/>
          <Partners/>
        </div>
      </div>
    </Screen>
  );
};

TrackAParcel.defaultProps = {
  content: {
    firstColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    },
    secondColumn: {
      title: 'Title Here',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eget commodo magna, quis rhoncus metus. Cras vulputate bibendum nulla, nec pulvinar nibh feugiat id. Quisque sit amet risus varius, lobortis urna a, aliquet justo. Nulla sapien purus, suscipit in tellus sit amet, laoreet ullamcorper magna.',
    }
  }
};
