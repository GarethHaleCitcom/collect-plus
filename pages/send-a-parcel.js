import React from 'react';
import Screen from '../components/screen';
import Partners from "../components/partners";
import Header from "../components/header";
import dynamic from "next/dynamic";

const StoreLocator = dynamic(() => import('../components/store-locator'), {ssr: false});
const Trustpilot = dynamic(() => import('../components/trustpilot'), {ssr: false});
export default function SendAParcel() {
  return (
    <Screen>
      <div className="send-a-parcel">
        <Header title="Send a Parcel" backgroundImage="/headers/blog-header.jpg"/>
        <div className="container-medium">
          <Partners styleType="title--orange"
                    sendLinks={true}
                    title="Send a parcel from any Collect+ store across the UK via our partners"
                    description="With PayPoint taking full ownership of the Collect+ brand, we are opening the Collect+ send service to all our courier partners:"/>
        </div>
        <StoreLocator storeLocatorTitle="Find a Store"/>
        <div className="container-medium">
          <Trustpilot/>
        </div>
      </div>
    </Screen>
  )
}
