import React from "react";
import "../styles/globals.scss";
import AppContextProvider from "../components/app-context-provider";

export default function App({ Component, pageProps }) {
    return (
        <AppContextProvider>
            <Component {...pageProps} />
        </AppContextProvider>
    );
}
