import React, {useContext} from 'react';
import Screen from "../components/screen";
import Header from "../components/header";
import Feedback from "../components/feedback";
import BlogsSection from "../components/blogs-section";
import {AppContext} from "../config/context";
import BlogCardPreview from "../components/blog-card-preview";
import Partners from "../components/partners";
import Trustpilot from "../components/trustpilot";
import ScrollAnimation from "../components/scroll-animation";
import dynamic from 'next/dynamic';

const StoreLocator = dynamic(() => import('../components/store-locator'), {ssr: false});

export default function Home({meta}) {
  const {blogs} = useContext(AppContext);
  return (
    <Screen name="home" meta={meta}>
      <div className="home">
        <Header backgroundImage="/headers/home-header.jpg" title="Welcome"/>
        <section className="home__find-a-store" id="find-a-store">
          <StoreLocator onlyStoreLocator={false}/>
        </section>
        <div className="container-medium">
          <h1 className="home__trust-pilot-title">
            Review your Collect+ experience via Trustpilot
          </h1>
          <p className="home__trust-pilot-description">
            Any reviews you would like to leave about the delivery service/parcel, please review this on the courier or
            retailers Trustpilot page.
          </p>
          <Trustpilot/>
        </div>
        <section className="home__feedback">
          <ScrollAnimation className="container-medium">
            {/*<Feedback id="fade-in-on-scroll"/>*/}
            <BlogsSection>
              {
                blogs.map(({data: {card: {title, description, imageSrc}, slug}}, key) => {
                  return (
                    <BlogCardPreview key={key} description={description} slug={slug} title={title} imageSrc={imageSrc}/>
                  )
                })
              }
            </BlogsSection>
          </ScrollAnimation>
        </section>
        <div className="container-medium">
          <Partners/>
        </div>
      </div>
    </Screen>
  )
};

Home.defaultProps = {
  title: 'Collect+',
  description: 'Parcels made easy. Send, collect and return parcels at your local store from early till late, 7 days a week. Free tracking and cover available as standard.',
};
