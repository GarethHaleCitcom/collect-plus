export default new class Storage {
  get(name) {
    const encodedResults = localStorage.getItem(name);
    if(!encodedResults) {return null}
    const decodedResults = atob(encodedResults);
    return JSON.parse(decodedResults);
  }
  set(name, data) {
    const encoded = btoa(JSON.stringify(data));
    localStorage.setItem(name, encoded);
    return this.get(name);
  }
}
