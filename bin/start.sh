#!/usr/bin/env bash

eval $(egrep -v "^#" .env | xargs) MYCOMMAND

if [[ "$NODE_ENV" = development ]]
then
  npm run server:watch &
  npm run client:watch
else
  npm run client:build
  npm run server:restart | npm run server:start
fi
