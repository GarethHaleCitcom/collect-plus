module.exports = function parcelRequest(sequelize, DataTypes) {
  const schema = {
    id: {
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true,
    },
    trackingCode: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    expiry: {
      type: DataTypes.BIGINT,
    }
  };
  return sequelize.define("parcelRequest", schema, {
    tableName: "parcelRequests",
    indexes: [
      {
        unique: true,
        fields: ["trackingCode", "id"],
      },
    ],
  });
};
