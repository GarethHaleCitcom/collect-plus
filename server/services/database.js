const Sequelize = require("sequelize");
const config = require("../config");

module.exports = new Sequelize(
  config.db.database,
  config.db.username,
  config.db.password,
  {
    operatorsAliases: "0",
    logging: false,
    host: config.db.host,
    dialect: "mysql",
    pool: {
      min: 0,
      max: 10,
      acquire: 30000,
      idle: 10000,
    },
  }
);
