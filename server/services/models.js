const database = require("./database");

const models = {
  User: database.import("user", require("../models/user")),
  ParcelRequest: database.import("parcel-request", require("../models/parcel-request")),
};

function toJSON() {
  const { protected: protectedFields = [] } = this._modelOptions;
  const values = Object.assign({}, this.get());
  protectedFields.forEach((field) => {
    if (values.hasOwnProperty(field)) {
      delete values[field];
    }
  });
  return values;
}

for (const model in models) {
  if (!models.hasOwnProperty(model)) {
    continue;
  }
  models[model].prototype.toJSON = toJSON;
}

module.exports = models;
