const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const helmet = require("helmet");
const fileUpload = require("express-fileupload");
const config = require("../config");
const users = require("../api/users");
const parcelTracking = require("../api/parcel-tracker");
const notFound = require("../api/not-found");

const app = express();

app.use(helmet());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
  fileUpload({
    limits: { fileSize: config.files.maxSize },
    useTempFiles: true,
    tempFileDir: config.files.tmpDir,
  })
);
app.use("/api/users", users);
app.use("/api/parcel-tracker", parcelTracking);

app.all("/api/*", notFound);

module.exports = app;
