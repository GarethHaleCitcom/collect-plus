const moment = require("moment");

const timestamp = () => moment().format("YYYY-MM-DD HH:mm:ss");

module.exports = {
  log(...args) {
    console.log(`[${timestamp()}]`, ...args);
  },
  error(...args) {
    console.error(`[${timestamp()}]`, ...args);
  },
};
