const logger = require("./logger");
const database = require("./database");

module.exports = async function bootstrap() {
  try {
    require("./models");
    await database.sync({ alter: true });
  } catch (err) {
    logger.error(err);
  }
};
