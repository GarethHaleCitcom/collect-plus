const { Router } = require("express");
const create = require("../actions/parcel-requests/create");
const get = require("../actions/parcel-requests/get");
const router = new Router();

router.get("/", get);

module.exports = router;
