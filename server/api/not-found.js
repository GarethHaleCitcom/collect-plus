const { Router } = require("express");
const notFound = require("../actions/not-found/not-found");

const router = new Router();

router.all("*", notFound);

module.exports = router;
