const { Router } = require("express");
const all = require("../actions/users/all");

const router = new Router();

router.get("/", all);

module.exports = router;
