const path = require("path");
require("dotenv").config({ path: path.resolve(__dirname, "..", ".env") });

module.exports = {
  port: process.env.SERVER_PORT || 3000,
  db: {
    host: process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
  },
  paths: {
    storage: path.resolve(__dirname, "..", "storage"),
  },
  files: {
    maxSize: process.env.FILE_MAX_SIZE,
    tmpDir: process.env.FILE_TMP_DIR,
  },
};
