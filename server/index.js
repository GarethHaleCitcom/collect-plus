const config = require("./config");
const bootstrap = require("./services/bootstrap");
const app = require("./services/app");
const logger = require("./services/logger");

bootstrap().then(async () => {
  try {
    await app.listen(config.port);
    logger.log(`API running at http://localhost:${config.port}`);
  } catch (err) {
    logger.error(err);
  }
});
