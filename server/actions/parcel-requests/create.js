const logger = require("../../services/logger");
const { ParcelRequest } = require("../../services/models");

module.exports = async function create(req, res) {
  try {
    const users = await ParcelRequest.findAll();
    res.json({ data: users });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ errors: { server: "Server error" } });
  }
};
