const logger = require("../../services/logger");
const { User } = require("../../services/models");

module.exports = async function all(req, res) {
  try {
    const users = await User.findAll();
    res.json({ data: users });
  } catch (err) {
    logger.error(err);
    res.status(500).json({ errors: { server: "Server error" } });
  }
};
