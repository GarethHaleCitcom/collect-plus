module.exports = async function notFound(req, res) {
  res.status(404).json({ errors: { server: "Endpoint not found" } });
};
