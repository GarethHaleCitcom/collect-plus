export default {
    minZoom: 1,
    maxZoom: 50,
    initialZoom: 14,
    startLocation: {
        lat: 51.505,
        lng: -0.09,
    },
}
