
const partnerFactory = props => {
  return {
    imageSrc: `/partners/${props.filename}`,
    link: props.link,
    name: props.name,
    id: props.id,
    sendLink: props.sendLink || null,
  }
};

export default [
  partnerFactory({
    filename: 'yodel.svg',
    link: 'https://www.collectplus.yodel.co.uk/',
    name: 'Yodel',
    id: 'yodel',
    sendLink: 'https://www.collectplus.yodel.co.uk/send-a-parcel',
  }),
  partnerFactory({
    filename: 'dhl.png',
    link: 'https://www.dhlparcel.co.uk/',
    name: 'DHL',
    id: 'dhl',
    sendLink: 'https://send.dhlparcel.co.uk/',
  }),
  partnerFactory({
    filename: 'fedex.svg',
    link: 'https://www.fedex.com/en-gb/home.html?INTCMP=fedexcouk',
    name: 'FedEx',
    id: 'fedEx',
    sendLink: 'http://www.fedex.com/gb/how-to-ship/',
  }),
  partnerFactory({
    filename: 'parcel2Go.svg',
    link: 'https://www.parcel2go.com/',
    name: 'Parcel2Go',
    id: 'parcel2go',
    sendLink: 'https://www.parcel2go.com/smartsend',
  }),
]
