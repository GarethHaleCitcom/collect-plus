const menuLinkFactory = ({path, name}) => {
  return {
    path,
    name,
  }
};

export default [
  menuLinkFactory({
    path: '/',
    name: 'Welcome'
  }),
  menuLinkFactory({
    path: '/find-a-store',
    name: 'Find a Store'
  }),
  menuLinkFactory({
    path: '/track-a-parcel',
    name: 'Track a Parcel'
  }),
  menuLinkFactory({
    path: '/parcel-returns',
    name: 'Returns',
  }),
  menuLinkFactory({
    path: '/send-a-parcel',
    name: 'Send a Parcel',
  }),
  menuLinkFactory({
    path: '/help-and-advice',
    name: 'Help & Advice',
  }),
  // menuLinkFactory({
  //   path: '/blog',
  //   name: 'Blog',
  // }),
  // menuLinkFactory({
  //   path: '/about-us',
  //   name: 'About',
  // }),
  // menuLinkFactory({
  //   path: '/contact',
  //   name: 'Contact',
  // }),
];
