
export default {
  "amazon": {
    img: "/partners/amazon.svg",
    link: 'https://www.amazon.co.uk/',
  },
  "dhl": {
    img: "/partners/dhl.svg",
    link: "https://www.dhlparcel.co.uk/"
  },
  "deliveredExactly": {
    img: "/partners/delivered-exactly.png",
    link: "https://www.dxdelivery.com/",
  },
  "ebay": {
    img: "/partners/ebay.svg",
    link: "https://www.ebay.co.uk/",
  },
  "fedex": {
    img: "/partners/fedex.svg",
    link: "https://www.fedex.com/en-gb/home.html?INTCMP=fedexcouk",
  },
  "yodel": {
    img: "/partners/yodel.svg",
    link: "https://www.collectplus.yodel.co.uk/",
  },
  "parcel2go": {
    img: "/partners/parcel2Go.svg",
    link: "https://www.parcel2go.com/",
  },
}






