import slugify from "slugify";

const blogCategoryFactory = props => {
  return {
    id: slugify(props.name, {lower: true}),
    name: props.name,
  }
};
export default [
  blogCategoryFactory({
    name: 'Features',
  }),
  blogCategoryFactory({
    name: 'Competitions',
  }),
  blogCategoryFactory({
    name: 'Shopping Guide',
  }),
  blogCategoryFactory({
    name: 'Bag a Bargain',
  }),
  blogCategoryFactory({
    name: 'All Posts',
  })
]
