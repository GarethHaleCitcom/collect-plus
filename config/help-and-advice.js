const BASE_PATH = '/headers/';
const helpAndAdviceFactory = props => {
  return {
    title: props.title,
    list: props.list || null,
    description: props.description || null,
    secondDescription: props.secondDescription || null,
    imageSrc: props.imageSrc ? `${BASE_PATH}${props.imageSrc}` : null,
    secondTitle: props.secondTitle || null,
    secondList: props.secondList || null,
  }
};

export default [

  helpAndAdviceFactory({
    title: "How to track a parcel",
    description: '<p>Click & Collect – Once the courier has collected your parcel from the retailer you purchased the item from, you will usually be provided with a tracking code. The tracking code will enable you to track your parcel and will update to show when it is at the chosen Collect+ store. You can enter the tracking code on our <a target="_blank" href="/">website</a> and we will direct you to the relevant partners website.</p><p>Send/Return - Once your parcel has been scanned into one of our stores, you will receive a tracking code which will enable you to track your parcel and know once it has reached its destination.</p>',
    secondTitle: "Find a Collect+ store",
    secondDescription: `Please follow this <a href="/#find-a-store">link</a> to find a Collect+ store.`,
    imageSrc: 'help-and-support-1.jpg',

  }),
  helpAndAdviceFactory({
    title: "How do I get a collection code to collect my parcel?",
    description: '<p>Once your parcel has been delivered into store you will receive a collection code. The collection code will be sent to you via the method you’ve told your courier/retailer – i.e. email or SMS.</p><p>Where you had originally opted for home delivery and the carrier/retailer you are using has redirected the parcel to a Collect+ store, the courier/retailer that is responsible for your parcel will issue you with a collection code.</p>',
    imageSrc: 'help-and-support-2.jpg',
  }),
  helpAndAdviceFactory({
    title: "I visited a store and could not collect my parcel",
    list: [
      "The collection code you had may not have been valid",
      "You may have attempted to collect the parcel before it has been delivered/scanned as delivered in store",
      "You may have needed to provided ID for the collection of your parcel",
    ],
    description: 'There may be a few factors as to why you could not collect your parcel. Below are some of the most frequent reasons why:',
    secondDescription: 'In all cases please contact the courier/retailer that you have purchased the item from and they should be able to help you further.',
    imageSrc: 'help-and-support-3.jpg',
  }),

  helpAndAdviceFactory({
    title: "What shall I do if my parcel is lost or damaged?",
    description: "<p>If the parcel is lost and not in store on arrival, please contact the courier/retailer you purchased the item from.</p><p>If you have noticed the parcel is damaged in store on collection, please refuse collection. The Collect+ courier/retailer will arrange for the courier to return the parcel.</p>",
    imageSrc: 'help-and-support-4.jpg',
  }),

  helpAndAdviceFactory({
    title: "What ID should I have when collecting my parcel?",
    description: '<p>The ID required depends on the courier/retailer you have purchased the item from. Please check your email from the courier/retailer when collecting the item. The ID shown needs to match the name on the parcel.</p>',
    imageSrc: 'help-and-support-8.jpg',
    secondTitle: "How long do I have to collect my parcel?",
    secondDescription: '<p>This will depend on the partner that you have purchased the item from. We would encourage you to pick up your parcel at the earliest time that is convenient for you.</p>'
  }),
];
