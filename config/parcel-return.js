let id = 0;
const returnFactory = props => {
  id+=1;
  return {
    id: id,
    imageSrc: props.imageSrc,
    description: props.description,
  }
};

export default [
  returnFactory({
    description: 'Stick the Collect+ returns label on your package. This may have been in the packing with your item or you can request it from the retailer where you purchased the item.',
    imageSrc: '/return/return-0.svg'
  }),
  returnFactory({
    description: 'Drop it off at your local Collect+ store. Check the Find Store tab to check which stores accept which partners.',
    imageSrc: '/return/return-1.svg'
  }),
  returnFactory({
    description: 'Keep the receipt and track your return online.',
    imageSrc: '/return/return-2.svg'
  })
];
