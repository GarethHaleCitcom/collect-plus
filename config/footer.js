import {Instagram, Twitter, Linkedin} from "../components/social";

const linkFactory = props => {
  return {
    to: props.to,
    name: props.name
  }
};

const socialLinkFactory = props => {
  return {
    href: props.href,
    component: props.component
  }
};

export default {
  links: [
    [
      linkFactory({
        to: '/',
        name: 'Welcome'
      }),
      linkFactory({
        to: '/find-a-store',
        name: 'Find a Store'
      }),
      linkFactory({
        to: '/track-a-parcel',
        name: 'Track a Parcel'
      }),
      linkFactory({
        to: '/parcel-returns',
        name: 'Returns',
      }),
      linkFactory({
        to: '/send-a-parcel',
        name: 'Send a Parcel',
      }),
    ],
    [
      linkFactory({
        to: '/help-and-advice',
        name: 'Help & Advice',
      }),
      linkFactory({
        to: '/blogs',
        name: 'Blog',
      }),
      linkFactory({
        to: '/about',
        name: 'About',
      }),
      linkFactory({
        to: '/contact-us',
        name: 'Contact',
      }),
    ],
  ],
  bottomLinks: [
    linkFactory({
      to: '/privacy',
      name: 'Privacy Policy',
    }),
    linkFactory({
      to: '/terms',
      name: 'Terms and Conditions',
    }),
    linkFactory({
      to: '/cookies-policy',
      name: 'Cookies Policy',
    }),
  ],
  logoSrc: '/logo.svg',
  social: [
    socialLinkFactory({
      href: 'https://www.instagram.com/collectplus',
      component: Instagram,
    }),
    socialLinkFactory({
      href: 'https://twitter.com/CollectPlus',
      component: Twitter,
    }),
    socialLinkFactory({
      href: 'https://www.linkedin.com/company/collectplus/',
      component: Linkedin,
    })
  ]

}
