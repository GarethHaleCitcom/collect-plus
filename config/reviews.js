import moment from "moment";
const reviewFactory = props => {
  return {
    stars: props.stars,
    text: props.text,
    name: props.name,
    date: moment(props.date).format('MM DD YYYY'),
  }
};

export default [
  reviewFactory({
    stars: 5,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    name: 'Ianos Gyorgy',
  }),
  reviewFactory({
    stars: 5,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    name: 'Ianos Gyorgy',
  }),
  reviewFactory({
    stars: 5,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    name: 'Ianos Gyorgy',
  }),
  reviewFactory({
    stars: 5,
    text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
    name: 'Ianos Gyorgy',
  }),
];
