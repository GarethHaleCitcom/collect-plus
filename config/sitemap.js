import footer from "./footer";
import blogs from "./blogs";
import chunk from 'lodash/chunk';
import flatMapDeep from 'lodash/flatMapDeep';

export default flatMapDeep([
    ...footer.links,
    ...footer.bottomLinks,
    ...blogs.map((blog) => {
      return {
        to: `/blogs/${blog.data.slug}`,
        name: blog.meta.title,
      }
    })
]);
