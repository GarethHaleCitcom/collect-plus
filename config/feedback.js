
const feedbackFactory = props => {
  return {
    imageSrc: props.imageSrc || null,
    message: props.message,
    name: props.name,
    role: props.role || null
  }
};

export default [
  feedbackFactory({
    message: 'This option is really easy to use and the Collect+ shop is a two minute walk down the road from me which is a bonus. The lady in the shop was helpful and asked me for ID, so that reassured me that my parcel was safe until I arrived to pick it up.',
    name: 'S Hewitt',
  }),
  feedbackFactory({
    message: "This has to be one of the most trust worthy and efficient delivery services out there. I\'m an avid fan.",
    name: 'I O’Reilly',
  }),
  feedbackFactory({
    message: 'The Collect+ service is really useful. It is very convenient to take parcels to/from our local newsagents / convenience store. You can print your own labels and take items to return to the shop. Very convenient.',
    name: 'R Weeks',
  }),
  feedbackFactory({
    message: 'Very convenient and easy to print label and drop off for collection.',
    name: 'M Robertson',
  }),
  feedbackFactory({
    message: 'Using Collect+ was simple, convenient and efficient. Being able to easily track my return parcel was really useful.',
    name: 'K Purves',
  }),
]
